import json
import logging

from datetime import datetime
from collections import OrderedDict

import settings
from wurker.util import log, BotArgs
from wurker.core import Wurkman

from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, send_from_directory, jsonify, Response,
    current_app
)
from werkzeug.exceptions import abort

from app.auth import login_required
from app.db import get_db


bp = Blueprint('index', __name__)

logger = logging.getLogger('gunicorn.error')


@bp.route('/')
@bp.route('/drone/<drone_id>')
@login_required
def index(drone_id:str=None):
    """Get the Queen's Court view, for the Queen, herself, or one of her Drones."""

    if not settings.env.is_queen:
        return None, 404

    from wurker.subcommands.show import run as showcmd
    from wurker.subcommands.status import run as statuscmd

    # Will need the Queen's status for Node selectbox, even if this is a Drone
    queen_args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "status"
    )
    queen = statuscmd(queen_args)

    # Get this Wurker's status (Queen or Drone)
    status_args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "status",
        drone = [drone_id] if drone_id else None
    )

    status = statuscmd(status_args)
    info = (
        status if not drone_id
        else {} if not status or drone_id not in status
        else status[drone_id]
    )

    if "db_conf" in info:
        info["db_conf"]["password"] = "****"

    infoJson = json.dumps(info, sort_keys=True, indent=2)

    # Then get 'show' subcommand (Queen or Drone) with sections strictly sorted
    show_args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "show",
        any = True,
        drone = [drone_id] if drone_id else None
    )

    showman = Wurkman(show_args)
    show = showcmd(showman)

    bees = (
        show if not drone_id
        else {} if not show or drone_id not in show
        else show[drone_id]
    )

    sort_order = ["supervised", "scheduled", "ondemand", "orphaned", "disabled",]
    unsorted = {section:crons for section, crons in bees.items() if len(crons)}

    sections = OrderedDict()
    for section in [s for s in sort_order if s in unsorted]:
        sections[section] = unsorted[section]

    # UI bees need JSON args, not Python like we got with return_result=True
    for section, crons in sections.items():
        for cron, bees in crons.items():
            for slug, bee in bees.items():
                bee["args"] = json.dumps(bee["args"])

    # Then get the show hive data
    hive_args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "show",
        hive = True,
        drone = [drone_id] if drone_id else None
    )

    hiveman = Wurkman(hive_args)
    hives = showcmd(hiveman)

    hive = (
        hives if not drone_id
        else {} if not hives or drone_id not in hives
        else hives[drone_id]
    )

    # Assemble Hive data and add Ondemand into it, deleting from sections
    hive_data = {
        "hive": hive,
        "ondemand": sections.get("ondemand", {})
    }
    if "ondemand" in sections:
        del sections["ondemand"]

    # We also want the Crons and Commands for separate UI purposes
    cron_args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "show",
        cron = True,
        drone = [drone_id] if drone_id else None
    )

    cronman = Wurkman(cron_args)
    crons = showcmd(cronman)

    cron_data = (
        crons if not drone_id
        else {} if not crons or drone_id not in crons
        else crons[drone_id]
    )

    # Need this for modal Edit Cron form
    cron_dict = {v["slug"]:v for v in cron_data}
    cronJson = json.dumps(cron_dict, sort_keys=True, indent=2)

    cmd_args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "show",
        command = True,
        drone = [drone_id] if drone_id else None
    )

    cmdman = Wurkman(cmd_args)
    cmds = showcmd(cmdman)

    cmd_data = (
        cmds if not drone_id
        else {} if not cmds or drone_id not in cmds
        else cmds[drone_id]
    )

    # Need this for modal Edit Command form
    cmd_dict = {v["slug"]:v for v in cmd_data}
    commandJson = json.dumps(cmd_dict, sort_keys=True, indent=2)

    # We also want the "widowed" Crons and Commands for UI purposes
    cron_args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "show",
        cron = True,
        widowed = True,
        drone = [drone_id] if drone_id else None
    )

    cronman = Wurkman(cron_args)
    crons = showcmd(cronman)

    cron_widowed = (
        crons if not drone_id
        else {} if not crons or drone_id not in crons
        else crons[drone_id]
    )

    cmd_args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "show",
        command = True,
        widowed = True,
        drone = [drone_id] if drone_id else None
    )

    cmdman = Wurkman(cmd_args)
    cmds = showcmd(cmdman)

    cmd_widowed = (
        cmds if not drone_id
        else {} if not cmds or drone_id not in cmds
        else cmds[drone_id]
    )

    # TODO
    cmdman.session.remove()

    # Compose the result and render it
    result = {
        "page": info.get("name", None),              # For title
        "app_id": info.get("app_id", None),          # For API requests
        "is_queen": info.get("is_queen", None),      # For API requests, and just so you know
        "drones": queen.get("drones", None),         # For everything. They're everywhere.
        "env": info.get("env", None),                # Because DON'T FUCK UP PRODUCTION!
        "hiveData": hive_data,                       # UI display: What's abuzz?
        "cronData": cron_data,                       # For select options
        "commandData": cmd_data,                     # For select options
        "cronWidowed": cron_widowed,                 # For select options
        "commandWidowed": cmd_widowed,               # For select options
        "sections": sections,                        # For select options
        "infoJson": infoJson,                        # For node status
        "cronJson": cronJson,                        # For modal edit form
        "commandJson": commandJson,                  # For modal edit form
        "thisYear": datetime.now().strftime('%Y'),   # For copyright
    }

    return render_template('index.html', **result)
