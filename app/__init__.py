import os

import settings

from flask import Flask


def create_app(test_config=None):
    # create and configure the app
    instance_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data")
    app = Flask(__name__, instance_path=instance_path, instance_relative_config=True)

    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'app.db'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Init the app DB
    from . import db
    db.init_app(app)

    # Init the auth blueprint
    from . import auth
    app.register_blueprint(auth.bp)

    # Init the dashboard blueprint
    from . import index
    app.register_blueprint(index.bp)
    app.add_url_rule('/', endpoint='index')

    # Init the API blueprint
    from . import api
    app.register_blueprint(api.bp)

    # Init the reports blueprint
    from . import report
    app.register_blueprint(report.bp)

    return app
