from jinja2 import Environment, FileSystemLoader, select_autoescape, PackageLoader


class EmailClientBase:
    """Parent class for Wurker email utility, including Jinja2 and SMTP."""

    env = None
    template = None

    def __init__(self, template:str=''):
        """Initialize the environment to create templates in."""

        self.env = Environment(
            loader=PackageLoader('lib.mail'),
            autoescape=select_autoescape()
        )

        if template:
            self.set_template(template)

    def set_template(self, template:str):
        """Sets the current template, raising an Exception on error."""

        self.template = self.env.get_template(template)

    def render(self, template:str='', data:dict={}):
        """Render either using already set template, or the given one. Raises Exception on error."""

        if not self.template and not template:
            raise Exception("No template set: use `set_template()` first")
        if not data or not isinstance(data, dict):
            raise ValueError("No `data` dict provided")

        if template:
            self.set_template(template)

        return self.template.render(**data)

    def send(self, *args, **kwargs):
        """Prototype send function. TODO: Provide default SMTP send method."""

        raise NotImplementedError
