import os
import psutil
import logging

from datetime import datetime
from sqlalchemy.sql import select, and_

import settings
from wurker.util import log
from wurker.db import make_engine

from models.wurker import WurkerHive


class Hive:
    """Class to encapsulate and provide convenience for accessing local DB."""

    engine = None
    hive = None

    def __init__(self, app_root=None, clear=False):
        db_conf = settings.env.LocalDB
        db_file = os.path.join(app_root, db_conf.database) if app_root else os.path.abspath(db_conf.database)
        log(f"Connecting to local DB: '{db_file}'...")

        self.engine = make_engine(db_conf=db_conf)

        if clear:
            self.clear()

        self.sync()

    def clear(self):
        """Clear the active hive DB."""

        with self.engine.connect() as db:
            stmt = WurkerHive.__table__.delete()
            db.execute(stmt)

    def load(self):
        """Get bees currently active in the hive DB."""

        self.hive = {}
        log("Loading Wurker Hive from local DB", level=logging.DEBUG)

        with self.engine.connect() as db:
            stmt = select(WurkerHive)
            hive = db.execute(stmt).all()
            for row in hive:
                if row.wurker_cron_id not in self.hive:
                    self.hive[row.wurker_cron_id] = {}
                self.hive[row.wurker_cron_id][row.wurker_bee_id] = {
                    "pid": row.pid,
                    "timestamp": row.timestamp,
                    "runcount": row.runcount,
                }

    def get(self, bee_id, cron_id):
        """Get a bee from the hive by unique key."""

        with self.engine.connect() as db:
            stmt = select(WurkerHive).where(and_(
                WurkerHive.wurker_bee_id == bee_id,
                WurkerHive.wurker_cron_id == cron_id
            ))
            return db.execute(stmt).first()

    def add(self, bee:object):
        """Add a bee to the hive."""

        data = {
            "wurker_bee_id": bee.id,
            "wurker_cron_id": bee.cron_id,
            "pid": bee.pid,
            "runcount": bee.runcount,
            "timestamp": datetime.utcnow(),
        }

        with self.engine.connect() as db:
            stmt = WurkerHive.__table__.insert().prefix_with(" OR IGNORE").values(**data)
            db.execute(stmt)

    def set_runcount(self, bee:object):
        """Update the runcount for the given bee for display."""

        with self.engine.connect() as db:
            stmt = WurkerHive.__table__.update().values(runcount=bee.runcount)
            db.execute(stmt)

    def delete(self, bee:object=None, pid:int=None):
        """Delete a bee from the hive."""

        pid = bee.pid if bee is not None else pid
        if pid is None:
            return

        with self.engine.connect() as db:
            stmt = WurkerHive.__table__.delete().where(
                WurkerHive.pid == pid
            )
            db.execute(stmt)

    def sync(self):
        """Deletes Bee PIDs that aren't actually running."""

        log("Syncing Wurker Hive with running processes", level=logging.DEBUG)

        self.load()
        for cron_id, bee in self.hive.items():
            for bee_id, item in bee.items():
                if not psutil.pid_exists(item["pid"]):
                    self.delete(pid=item["pid"])
