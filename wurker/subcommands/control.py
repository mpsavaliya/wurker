import settings
from ..db import get_drone_status
from ..drone import make_drone_clients
from models.wurker import WurkerControl


def run_drone(args):
    """Run via Drone API(s)."""

    db_conf = settings.env.RemoteDB
    app_id = settings.env.app_id
    drone_status = get_drone_status(db_conf, app_id, drones=args.drone)
    if drone_status is None or not len(drone_status):
        print(f"WARNING: Queen ({app_id}) Drones have no 'wurker_status' data", file=sys.stderr)
        return

    drone_clients = make_drone_clients(drone_status, auth=settings.env.ApiAuth)

    for app_id, drone in drone_clients.items():
        drone.post_control(args.action)


def run(wurkman):
    """Control the Wurker Bee Hive from the CLI."""

    # Run via Drone API?
    if hasattr(wurkman.args, "drone") and wurkman.args.drone is not None:
        return run_drone(wurkman.args)

    if wurkman.args.action == "reload":
        wurkman.set_control(reload=True)
    elif wurkman.args.action == "pause":
        wurkman.set_control(pause=True)
