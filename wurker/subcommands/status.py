import os
import sys
import json
import socket
import subprocess

from sqlalchemy import func
from sqlalchemy.sql import select, join

import settings
from ..drone import make_drone_clients
from ..db import make_engine, schema_exists, get_drone_status
from models.wurker import Base, WurkerControl, WurkerBee, WurkerStatus


def run_drone(args):
    """Run via Drone API(s)."""

    db_conf = settings.env.RemoteDB
    app_id = settings.env.app_id
    drone_status = get_drone_status(db_conf, app_id, drones=args.drone)
    if drone_status is None or not len(drone_status):
        print(f"WARNING: Queen ({app_id}) Drones have no 'wurker_status' data", file=sys.stderr)
        return

    drone_clients = make_drone_clients(drone_status, auth=settings.env.ApiAuth)

    results = {}
    for app_id, drone in drone_clients.items():
        result = drone.get_status()

        if not result:
            print(f"WARNING: No 'status' result from Drone '{drone.slug}' ({drone.app_id})", file=sys.stderr)
            continue

        results[app_id] = result

    if not results:
        print(f"No 'status' results from Drones", file=sys.stderr)
        return

    if hasattr(args, "return_result") and args.return_result:
        return results

    # OK, everything else would've printed to stderr... this is the final successful output
    print(json.dumps(results, sort_keys=True, indent=2))


def run(args):
    """Display Wurker status."""

    # Run via Drone API?
    if hasattr(args, "drone") and args.drone is not None:
        return run_drone(args)

    return_result = hasattr(args, "return_result") and args.return_result

    def _get_private_ip(target_ip='8.8.8.8'):
        """Get the local IP for the NIC used to access the target_ip."""

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.connect((target_ip, 1))
        local_ip = sock.getsockname()[0]
        sock.close()
        return local_ip

    def _get_public_ip():
        """Perform a `curl ipinfo.io/ip` to get the public IP of this agent host."""

        cmd = ["curl", "ipinfo.io/ip"]
        try:
            proc = subprocess.run(
                cmd,
                stdout = subprocess.PIPE,
                stderr = subprocess.DEVNULL,
                check = True
            )
            public_ip = proc.stdout.decode("utf-8").strip()
        except:
            return None
        else:
            return public_ip

    def _print_status(status, return_result=False):
        if return_result:
            return status

        print(json.dumps(status, sort_keys=True, indent=2))

    status = {
        "app_id": None,
        "name": None,
        "slug": None,
        "is_queen": False,
        "env": None,
        "private_ip": None,
        "public_ip": None,
        "server_domain": None,
        "db_conf": None,
        "db_conn": False,
        "db_schema": False,
        "control": None,
        "bees": None,
        "drones": None,
        "status": False,
    }

    status["app_id"] = settings.env.app_id
    if not status["app_id"]:
        raise "Missing valid WURKER_APP_ID setting in .env"

    status["name"] = settings.env.name
    status["slug"] = settings.env.slug
    status["is_queen"] = settings.env.is_queen
    status["env"] = settings.env.env or "production"

    status["private_ip"] = _get_private_ip()
    status["public_ip"] = _get_public_ip()
    status["server_domain"] = settings.env.server_domain

    db_conf = settings.env.RemoteDB
    status["db_conf"] = vars(db_conf)

    # Make sure we can create a DB connection
    try:
        engine = make_engine(db_conf=db_conf)
        conn = engine.connect()
        status["db_conn"] = True
    except Exception as ex:
        return _print_status(status, return_result=return_result)

    # Make sure the schema exists
    try:
        schema_exists(db_conf, Base)
        status["db_schema"] = True
    except Exception as ex:
        return _print_status(status, return_result=return_result)

    # Make sure we have a control record
    stmt = select(WurkerControl).where(WurkerControl.app_id == status["app_id"])
    control = conn.execute(stmt).first()
    if control is None:
        return _print_status(status, return_result=return_result)

    status["control"] = {
        "reload": control.reload,
        "pause": control.pause,
        "seed": control.seed,
    }

    # Queens also get Drone control records
    if status["is_queen"]:
        stmt = select([WurkerControl, WurkerStatus]).select_from(join(
            WurkerControl, WurkerStatus, WurkerControl.app_id == WurkerStatus.app_id
        )).where(WurkerControl.app_id != status["app_id"])

        drones = conn.execute(stmt).all()
        if len(drones):
            status["drones"] = {}
            for drone in drones:
                status["drones"][str(drone.app_id)] = {
                    "slug": drone.slug,
                    "name": drone.name,
                    "is_queen": drone.is_queen,
                    "env": drone.env,
                    "private_ip": drone.private_ip,
                    "public_ip": drone.public_ip,
                    "server_domain": drone.server_domain,
                    "control": {
                        "app_id": str(drone.app_id),
                        "reload": drone.reload,
                        "pause": drone.pause,
                        "seed": drone.seed,
                    },
                }

    # Get counts for display
    bees_ct = {"enabled": 0, "disabled": 0,}

    stmt = select(func.count(WurkerBee.id).label("bee_ct")).where(
        WurkerBee.enabled == True
    ).where(
        WurkerBee.app_id == status["app_id"]
    )
    bees_ct["enabled"] = conn.execute(stmt).first().bee_ct

    stmt = select(func.count(WurkerBee.id).label("bee_ct")).where(
        WurkerBee.enabled == False
    ).where(
        WurkerBee.app_id == status["app_id"]
    )
    bees_ct["disabled"] = conn.execute(stmt).first().bee_ct

    status["bees"] = bees_ct

    # Now display the data sorta nice
    status["status"] = True
    return _print_status(status, return_result=return_result)
