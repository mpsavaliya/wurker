from sqlalchemy.exc import IntegrityError

import settings
from ..db import get_drone_status
from ..drone import make_drone_clients
from models.wurker import WurkerBee, WurkerCron, WurkerCommand, WurkerQueue


def run_drone(args):
    """Run via Drone API(s)."""

    db_conf = settings.env.RemoteDB
    app_id = settings.env.app_id
    drone_status = get_drone_status(db_conf, app_id, drones=args.drone)
    if drone_status is None or not len(drone_status):
        print(f"WARNING: Queen ({app_id}) Drones have no 'wurker_status' data", file=sys.stderr)
        return

    drone_clients = make_drone_clients(drone_status, auth=settings.env.ApiAuth)

    for app_id, drone in drone_clients.items():
        drone.post_remove(args.option, args.key)


def run(wurkman):
    """Remove Wurker Bee, Cron, or Map."""

    # Run via Drone API?
    if hasattr(wurkman.args, "drone") and wurkman.args.drone is not None:
        return run_drone(wurkman.args)

    with wurkman.session() as db:

        # TODO Remove Ondemand Bee(s)? ... list of slugs
        if wurkman.args.option == "ondemand":
            bee_slug = wurkman.args.key[0]
            bee = db.query(WurkerBee).filter(
                WurkerBee.slug == bee_slug,
                WurkerBee.app_id == wurkman.app_id
            ).first()

            if bee is None:
                raise ValueError(f"Bee not found: '{bee_slug}'")

            queued = db.query(WurkerQueue).filter(
                WurkerQueue.wurker_bee_id == bee.id,
                WurkerQueue.app_id == wurkman.app_id,
            ).all()

            if not len(queued):
                raise ValueError(f"Bee not found: '{bee_slug}")

            for item in queued:
                db.delete(item)

            db.commit()
            return

        # Gotta pre-process params in case they used slugs instead of IDs
        nomap = {"bee": WurkerBee, "cron": WurkerCron, "command": WurkerCommand}

        # Remove Commands, Bees, and Crons
        if wurkman.args.option != "map":
            model = nomap[wurkman.args.option]
            key = wurkman.args.key[0]
            if not key.isnumeric():
                item = db.query(model).filter(
                    model.slug == key,
                    model.app_id == wurkman.app_id
                ).first()

                if item is None:
                    raise ValueError(f"Cannot remove {wurkman.args.option}: No record found with slug '{key}'")

                wurkman.args.key[0] = item.id

            stmt = model.__table__.delete().where(model.id == wurkman.args.key[0])

            # Can't delete stuff if stuff is still using it
            if wurkman.args.option == "command" and len(item.bees):
                raise ValueError(f"Cannot remove WurkerCommand '{item.slug}': It has ({len(item.bees)}) Bees")
            elif wurkman.args.option == "bee" and len(item.crons):
                raise ValueError(f"Cannot remove WurkerBee '{item.slug}': run `rm map BEE CRON` first")
            elif wurkman.args.option == "cron" and len(item.bees):
                raise ValueError(f"Cannot remove WurkerCron '{item.slug}': run `rm map BEE CRON` first")

            try:
                db.execute(stmt)
                db.commit()
            except Exception as ex:
                db.rollback()
                raise ex

            return

        # Remove a map
        if len(wurkman.args.key) != 2:
            raise ValueError("Cannot map: Set PK (BEE CRON) as the KEY KEY")

        # First deal with possible slugs in the map KEYs ...
        models = list(nomap.values())
        for i, key in enumerate(wurkman.args.key):
            if key.isnumeric():
                continue

            query = db.query(models[i]).filter(models[i].slug == key)
            if not isinstance(models[i], WurkerCommand):
                query = query.filter(models[i].app_id == wurkman.app_id)

            item = query.first()
            if item is None:
                raise ValueError(f"Cannot map: No {models[i]} found with slug '{key}'")

            wurkman.args.key[i] = item.id

        # A map? Then a little trickier...
        bee_id, cron_id = wurkman.args.key

        bee = db.query(WurkerBee).filter(
            WurkerBee.id == bee_id,
            WurkerBee.app_id == wurkman.app_id
        ).first()

        if bee is None:
            raise ValueError(f"Cannot find WurkerBee with ID '{bee_id}'")

        cron = db.query(WurkerCron).filter(
            WurkerCron.id == cron_id,
            WurkerCron.app_id == wurkman.app_id
        ).first()

        if cron is None:
            raise ValueError(f"Cannot find WurkerCron with ID '{cron_id}'")

        bee.crons.remove(cron)
        db.commit()
