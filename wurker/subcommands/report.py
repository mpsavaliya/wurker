import json

from sqlalchemy import text

import settings
from ..db import get_drone_status
from ..drone import make_drone_clients
from ..util import DateTimeEncoder, str_to_datetime
from models.wurker import WurkerBeeReport, WurkerBee, WurkerCron


def run_drone(args):
    """Run via Drone API(s)."""

    db_conf = settings.env.RemoteDB
    app_id = settings.env.app_id
    drone_status = get_drone_status(db_conf, app_id, drones=args.drone)
    if drone_status is None or not len(drone_status):
        print(f"WARNING: Queen ({app_id}) Drones have no 'wurker_status' data", file=sys.stderr)
        return

    drone_clients = make_drone_clients(drone_status, auth=settings.env.ApiAuth)

    results = {}
    for app_id, drone in drone_clients.items():
        result = drone.get_report(slug=args.key, start=args.start, end=args.end)

        if result is None:
            print(f"WARNING: No 'report' result from Drone '{drone.slug}' ({drone.app_id})", file=sys.stderr)
            continue

        results[app_id] = result

    if not results:
        print(f"No 'report' results from Drones", file=sys.stderr)
        return

    if hasattr(args, "return_result") and args.return_result:
        return results

    # OK, everything else would've printed to stderr... this is the final successful output
    print(json.dumps(results, sort_keys=True, indent=2))


def run(wurkman):
    """View Wurker Report."""

    # Run via Drone API?
    if hasattr(wurkman.args, "drone") and wurkman.args.drone is not None:
        return run_drone(wurkman.args)

    # Validate start/end if set
    start_dt = None
    if hasattr(wurkman.args, "start") and wurkman.args.start is not None:
        start_dt = str_to_datetime(wurkman.args.start)

    end_dt = None
    if hasattr(wurkman.args, "end") and wurkman.args.end is not None:
        end_dt = str_to_datetime(wurkman.args.end)

    # Validate sort column
    report_columns = [
        'id', 'bee_id', 'bee_slug', 'bee_name', 'cron_id', 'cron_slug', 'cron_name',
        'hive_pid', 'hive_timestamp', 'hive_runcount', 'hive_duration', 'status',
        'created_at', 'updated_at',
    ]
    if wurkman.args.sort not in report_columns:
        raise Exception(f"Unknown sort column: '{wurkman.args.sort}'")

    # OK get on with it!
    data = []
    with wurkman.session() as db:

        # We know this much for sure...
        query = db.query(WurkerBeeReport).filter(
            WurkerBeeReport.app_id == wurkman.app_id
        ).order_by(WurkerBeeReport.id.asc())

        # Filter the query?
        if wurkman.args.option == "bee" and wurkman.args.key is not None:
            query = query.filter(WurkerBeeReport.bee.has(WurkerBee.slug == wurkman.args.key))
        elif wurkman.args.option == "cron" and wurkman.args.key is not None:
            query = query.filter(WurkerBeeReport.cron.has(WurkerCron.slug == wurkman.args.key))

        # If set, query daterange. Use `created` because `hive_timestamp` may be NULL
        if start_dt and end_dt:
            query = query.filter(
                WurkerBeeReport.created_at >= start_dt,
                WurkerBeeReport.created_at < end_dt
            )

        # Paginate the query
        total = query.count()
        begin = wurkman.args.page * wurkman.args.page_limit
        end = begin + wurkman.args.page_limit

        # Actually run the paginated query
        for row in query[begin:end]:
            data.append({
                "id": row.id,
                "bee_id": row.wurker_bee_id,
                "bee_slug": row.bee.slug,
                "bee_name": row.bee.name,
                "cron_id": row.wurker_cron_id,
                "cron_slug": row.cron.slug if row.wurker_cron_id else None,
                "cron_name": row.cron.name if row.wurker_cron_id else None,
                "hive_pid": row.hive_pid,
                "hive_timestamp": row.hive_timestamp.isoformat() if row.hive_timestamp else None,
                "hive_runcount": row.hive_runcount,
                "hive_duration": row.hive_duration,
                "status": row.status.name,
                "status_info": row.status_info,
                "created_at": row.created_at.isoformat(),
                "updated_at": row.updated_at.isoformat(),
            })

    # Now sort the results
    reverse = (wurkman.args.sort_order == "desc")
    data = sorted(data, key=lambda x: x[wurkman.args.sort], reverse=reverse)

    # We have the data, now always structure for possible pagination
    result = {
        "total": total,
        "page": wurkman.args.page,
        "limit": wurkman.args.page_limit,
        "data": data,
    }

    if hasattr(wurkman.args, "return_result") and wurkman.args.return_result:
        return result

    print(json.dumps(result, indent=2, cls=DateTimeEncoder))
