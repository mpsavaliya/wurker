import os
import shutil
import subprocess

import settings


def run(args):
    """Configure and install systemd services."""

    etc_dir = os.path.join(args.app_root, "etc")
    install_all = (not args.wurker_only and not args.ui_only)

    if not args.domain and (install_all or args.ui_only):
        return "ERROR: The --domain option is required to configure 'wurker-ui'"

    print(f"==> Configuring and installing systemd service(s) from: '{etc_dir}'...")

    if install_all or args.wurker_only:
        _install(args, etc_dir, "wurker")

    if install_all or args.ui_only:
        _install(args, etc_dir, "ui")


def _install(args, etc_dir, service):
    """Render the template and copy service config into place."""

    # Render wurker or wurker-ui systemd conf
    service_conf = _render_systemd_conf(args, etc_dir, service)

    # If wurker-ui, then also render the nginx vhost file
    vhost_conf = _render_nginx_conf(args, etc_dir) if service == "ui" else None

    # Just config? Then we're done
    if not args.install:
        return

    # Make sure they're rooted
    if os.geteuid() != 0:
        raise Exception("You must be a root user. Try: 'sudo !!'")

    # OK we're installing, too...
    systemd_dir = os.path.join("/etc", "systemd")
    if not os.path.exists(systemd_dir):
        raise Exception("Wurker installation requires systemd")

    shutil.copy(service_conf, os.path.join(systemd_dir, "system"))
    print(f"  . Installed systemd service file to: '{systemd_dir}'")

    if service == "ui" and vhost_conf:
        # Install javascript deps for Queen only
        if settings.env.is_queen:
            static_dir = os.path.join(os.path.dirname(etc_dir), "app", "static")
            subprocess.run(f"cd {static_dir} && npm install", shell=True, check=True)

        # Install nginx conf
        nginx_dir = os.path.join("/etc", "nginx")
        if not os.path.exists(nginx_dir):
            raise Exception("Wurker UI installation requires nginx")

        nginx_sites = os.path.join(nginx_dir, "sites-available")
        shutil.copy(vhost_conf, nginx_sites)
        print(f"  . Installed Wurker UI nginx vhost file to: '{nginx_sites}'")

        # Install nginx SSL - only if they exist
        nginx_ssl = os.path.join(nginx_dir, "ssl")
        if not os.path.exists(nginx_ssl):
            os.makedirs(nginx_ssl)

        ssl_certs = [os.path.join(args.ssl_dir, c) for c in args.ssl_certs]
        if not all(os.path.exists(c) for c in ssl_certs):
            raise Exception(f"Missing SSL cert(s): {ssl_certs}")

        for cert in ssl_certs:
            shutil.copy(cert, nginx_ssl)


def _render_systemd_conf(args, etc_dir, service):
    """Render the systemd service file for wurker or wurker-ui."""

    systemd_skel_dir = os.path.join(etc_dir, "systemd")
    service_conf = "wurker.service" if service == "wurker" else "wurker-ui.service"
    service_skel = f"{service_conf}.skel"

    skel = None
    with open(os.path.join(systemd_skel_dir, service_skel), 'r') as f:
        skel = f.read()

    if not skel:
        return f"ERROR: systemd template file empty: '{service_skel}'"

    skel = skel.replace("{{ app_root }}", args.app_root)
    skel = skel.replace("{{ virtualenv }}", args.virtualenv)
    skel = skel.replace("{{ workingdir }}", args.workingdir)

    if service == "ui":
        skel = skel.replace("{{ workers }}", str(args.workers))
        skel = skel.replace("{{ socket }}", args.socket)

    service_conf = os.path.join(systemd_skel_dir, service_conf)
    with open(service_conf, 'w') as f:
        f.write(skel)

    print(f"  . Rendered systemd service file from template: '{service_conf}'")

    return service_conf


def _render_nginx_conf(args, etc_dir):
    """Render the nginx vhost file for wurker-ui."""

    # Vhost stuff
    vhost_skel_dir = os.path.join(etc_dir, "nginx")
    vhost_conf = f"{args.domain}.conf"
    vhost_skel = "wurker-ui.conf.skel"

    # SSL certs stuff
    ssl_dir = os.path.join(etc_dir, "nginx", "ssl")
    if not os.path.exists(ssl_dir):
        os.makedirs(ssl_dir, mode=0o755)

    # TODO Generate self-signed certs if certs don't exist
    ssl_certs = [os.path.join(args.ssl_dir, c) for c in args.ssl_certs]
    ssl_crt, ssl_key = ssl_certs
    if not os.path.exists(ssl_crt) or not os.path.exists(ssl_crt):
        _create_certs(args.ssl_dir, ssl_crt, ssl_key)

    # Load the skel template
    skel = None
    with open(os.path.join(vhost_skel_dir, vhost_skel), 'r') as f:
        skel = f.read()

    if not skel:
        raise Exception(f"ERROR: nginx template file empty: '{vhost_skel}'")

    skel = skel.replace("{{ domain }}", args.domain)
    skel = skel.replace("{{ socket }}", args.socket)
    skel = skel.replace("{{ ssl_certificate }}", args.ssl_certs[0])
    skel = skel.replace("{{ ssl_certificate_key }}", args.ssl_certs[1])

    vhost_conf = os.path.join(vhost_skel_dir, vhost_conf)
    with open(vhost_conf, 'w') as f:
        f.write(skel)

    print(f"  . Rendered nginx vhost file from template: '{vhost_conf}'")

    return vhost_conf


def _create_certs(ssl_dir, ssl_crt, ssl_key):
    """Create SSL certs for nginx."""

    print(f"  . Creating self-signed SSL certificates in: '{ssl_dir}'")

    if not os.path.exists(ssl_dir):
        os.makedirs(ssl_dir)

    cmd = (
        "openssl req -nodes -newkey rsa:2048 -new -x509 "
        f"-out {ssl_crt} -keyout {ssl_key} "
        "-subj '/C=US/ST=FL/L=Miami/O=Wurkzen/OU=Wurker/CN=wurkzen.com/emailAddress=info@wurkzen.com'"
    )

    subprocess.run(cmd, shell=True, check=True)
