import settings
from ..util import parse_fillable
from ..db import get_drone_status
from ..drone import make_drone_clients
from models.wurker import WurkerBee, WurkerCron, WurkerCommand, WurkerQueue, WurkerBeeState


def run_drone(args):
    """Run via Drone API(s)."""

    db_conf = settings.env.RemoteDB
    app_id = settings.env.app_id
    drone_status = get_drone_status(db_conf, app_id, drones=args.drone)
    if drone_status is None or not len(drone_status):
        print(f"WARNING: Queen ({app_id}) Drones have no 'wurker_status' data", file=sys.stderr)
        return

    drone_clients = make_drone_clients(drone_status, auth=settings.env.ApiAuth)

    for app_id, drone in drone_clients.items():
        drone.post_add(args.option, data=args.json)


def run(wurkman):
    """Add Wurker Bee, Cron, or Map."""

    # Run via Drone API?
    if hasattr(wurkman.args, "drone") and wurkman.args.drone is not None:
        return run_drone(wurkman.args)

    if not wurkman.args.json:
        raise ValueError(f"You must set the key-values for the {wurkman.args.option} to add")

    with wurkman.session() as db:

        # Add Ondemand Bee(s) ... list of slugs or list of dicts like {slug:args}
        if wurkman.args.option == "ondemand":
            for item in wurkman.args.json:

                # TODO Ondemand override args are on you, bih
                bee_slug = None
                ondemand_args = None
                if isinstance(item, dict):
                    bee_slug = list(item.keys()).pop()
                    ondemand_args = item[bee_slug]
                else:
                    bee_slug = item

                if not bee_slug:
                    raise ValueError(f"Bee not found: '{bee_slug}'")

                bee = db.query(WurkerBee).filter(
                    WurkerBee.slug == bee_slug,
                    WurkerBee.app_id == wurkman.app_id,
                ).first()

                if bee is None:
                    raise ValueError(f"Bee not found: '{bee_slug}'")

                # Prohibit double-booking
                queued = db.query(WurkerQueue).filter(
                    WurkerQueue.wurker_bee_id == bee.id,
                    WurkerQueue.app_id == wurkman.app_id,
                ).count()

                if queued:
                    raise ValueError(f"Bee '{bee_slug}' is already queued")

                record = WurkerQueue(
                    wurker_bee_id = bee.id,
                    app_id = wurkman.app_id,
                    args = ondemand_args,
                )
                db.add(record)

            db.commit()
            return

        # Add Commands, Bees, and Crons
        if wurkman.args.option != "map":
            wurkman.args.cliargs = wurkman.args.json
            model, kwargs = parse_fillable(wurkman, enforce_required=True)

            query = db.query(model).filter(
                model.slug == kwargs["slug"],
                model.app_id == wurkman.app_id
            )

            if query.count():
                raise ValueError(f"A {wurkman.args.option} already exists for slug: '{kwargs['slug']}'")

            stmt = model.__table__.insert().values(**kwargs)
            db.execute(stmt)
            db.commit()

            return

        # Add a map

        if len(wurkman.args.json) != 2:
            raise ValueError(
                "MAP must be valid JSON with two keys: 'wurker_bee_id' and 'wurker_cron_id', "
                "or 'wurker_bee_slug' and 'wurker_cron_slug"
            )

        bee_query = db.query(WurkerBee).filter(WurkerBee.app_id == wurkman.app_id)
        cron_query = db.query(WurkerCron).filter(WurkerCron.app_id == wurkman.app_id)

        # Get from IDs or slugs?
        if "wurker_bee_slug" in wurkman.args.json and "wurker_cron_slug" in wurkman.args.json:
            bee_query = bee_query.filter(WurkerBee.slug == wurkman.args.json["wurker_bee_slug"])
            cron_query = cron_query.filter(WurkerCron.slug == wurkman.args.json["wurker_cron_slug"])
        elif "wurker_bee_id" in wurkman.args.json and "wurker_cron_id" in wurkman.args.json:
            bee_query = bee_query.filter(WurkerBee.id == wurkman.args.json["wurker_bee_id"])
            cron_query = cron_query.filter(WurkerCron.id == wurkman.args.json["wurker_cron_id"])
        else:
            raise ValueError("Both 'wurker_bee_id' and 'wurker_cron_id' must be set in the MAP")

        bee = bee_query.first()
        if bee is None:
            raise ValueError(f"Cannot find WurkerBee matching: {wurkman.args.json}")

        cron = cron_query.first()
        if cron is None:
            raise ValueError(f"Cannot find WurkerCron: {wurkman.args.json}")

        bee.crons.append(cron)
        db.commit()
