from sqlalchemy import select

import settings
from ..util import parse_fillable
from ..db import get_drone_status, make_engine
from ..drone import make_drone_clients
from models.wurker import WurkerBee, WurkerCron, WurkerCommand, ApiAuth


def run_drone(args):
    """Run via Drone API(s)."""

    db_conf = settings.env.RemoteDB
    app_id = settings.env.app_id
    drone_status = get_drone_status(db_conf, app_id, drones=args.drone)
    if drone_status is None or not len(drone_status):
        print(f"WARNING: Queen ({app_id}) Drones have no 'wurker_status' data", file=sys.stderr)
        return

    drone_clients = make_drone_clients(drone_status, auth=settings.env.ApiAuth)

    for app_id, drone in drone_clients.items():
        drone.post_edit(option=args.option, key=args.key, data=args.json)


def run(wurkman):
    """Edit Wurker Bees."""

    # Run via Drone API?
    if hasattr(wurkman.args, "drone") and wurkman.args.drone is not None:
        return run_drone(wurkman.args)

    if not wurkman.args.json:
        raise ValueError(f"You must set the key-values for the {wurkman.args.option} to edit")

    nomap = {"bee": WurkerBee, "cron": WurkerCron, "command": WurkerCommand}

    with wurkman.session() as db:

        # Edit Commands, Bees, and Crons
        if wurkman.args.option != "map":
            wurkman.args.cliargs = wurkman.args.json
            model = nomap[wurkman.args.option]
            key = wurkman.args.key[0]
            if wurkman.args.option != "command" and not key.isnumeric():
                item = db.query(model).filter(
                    model.slug == key,
                    model.app_id == wurkman.app_id
                ).first()

                if item is None:
                    raise ValueError(f"Cannot edit {wurkman.args.option}: No record found with slug '{key}'")

                wurkman.args.key[0] = item.id

            model, kwargs = parse_fillable(wurkman)
            if wurkman.args.option != "command":
                stmt = model.__table__.update().values(**kwargs).where(model.id == wurkman.args.key[0])
            else:
                stmt = model.__table__.update().values(**kwargs).where(model.slug == wurkman.args.key[0])

            db.execute(stmt)
            db.commit()

            return

        # Edit a map
        if len(wurkman.args.key) != 2:
            raise ValueError("Cannot map: Set PK (BEE CRON) as the KEY KEY")

        # Gotta pre-process params in case they used slugs instead of IDs
        models = list(nomap.values())
        for i, key in enumerate(wurkman.args.key):
            if key.isnumeric():
                continue

            query = db.query(models[i]).filter(models[i].slug == key)
            if not isinstance(models[i], WurkerCommand):
                query = query.filter(models[i].app_id == wurkman.app_id)

            item = query.first()
            if item is None:
                raise ValueError(f"Cannot map: No {models[i]} found with slug '{key}'")

            wurkman.args.key[i] = item.id

        # ... then deal with possible slugs in the cliargs (json or set dict)
        if len(wurkman.args.json) != 1:
            raise ValueError("Cannot map: JSON should only include the target Bee or Cron to move to")

        if (("wurker_bee_id" not in wurkman.args.json and "wurker_cron_id" not in wurkman.args.json)
            and ("wurker_bee_slug" not in wurkman.args.json and "wurker_cron_slug" not in wurkman.args.json)):
            raise ValueError("Cannot map: Set either 'wurker_bee_id' or 'wurker_cron_id' to edit the MAP")

        # Get the existing mapped IDs
        curr_bee_key, curr_cron_key = wurkman.args.key

        curr_bee_query = db.query(WurkerBee).filter(WurkerBee.app_id == wurkman.app_id)
        if isinstance(curr_bee_key, int):
            curr_bee_query = curr_bee_query.filter(WurkerBee.id == curr_bee_key)
        else:
            curr_bee_query = curr_bee_query.filter(WurkerBee.slug == curr_bee_key)

        curr_bee = curr_bee_query.first()
        if curr_bee is None:
            raise ValueError(f"Cannot find WurkerBee with ID '{curr_bee_id}'")

        curr_cron_query = db.query(WurkerCron).filter(WurkerCron.app_id == wurkman.app_id)
        if isinstance(curr_cron_key, int):
            curr_cron_query = curr_cron_query.filter(WurkerCron.id == curr_cron_key)
        else:
            curr_cron_query = curr_cron_query.filter(WurkerCron.slug == curr_cron_key)

        curr_cron = curr_cron_query.first()
        if curr_cron is None:
            raise ValueError(f"Cannot find WurkerCron with ID '{curr_cron_id}'")

        if "wurker_cron_id" in wurkman.args.json or "wurker_cron_slug" in wurkman.args.json:
            new_cron_query = db.query(WurkerCron).filter(WurkerCron.app_id == wurkman.app_id)
            if "wurker_cron_id" in wurkman.args.json:
                new_cron_query = new_cron_query.filter(WurkerCron.id == wurkman.args.json["wurker_cron_id"])
            else:
                new_cron_query = new_cron_query.filter(WurkerCron.slug == wurkman.args.json["wurker_cron_slug"])

            new_cron = new_cron_query.first()
            if new_cron is None:
                raise ValueError(f"Cannot find WurkerCron matching: {wurkman.args.json}")

            curr_bee.crons.remove(curr_cron)
            curr_bee.crons.append(new_cron)

        elif "wurker_bee_id" in wurkman.args.json or "wurker_bee_slug" in wurkman.args.json:
            new_bee_query = db.query(WurkerBee).filter(WurkerBee.app_id == wurkman.app_id)
            if "wurker_bee_id" in wurkman.args.json:
                new_bee_query = new_bee_query.filter(WurkerBee.id == wurkman.args.json["wurker_bee_id"])
            else:
                new_bee_query = new_bee_query.filter(WurkerBee.slug == wurkman.args.json["wurker_bee_slug"])

            new_bee = new_bee_query.first()
            if new_bee is None:
                raise ValueError(f"Cannot find WurkerBee matching: {wurkman.args.json}")

            curr_cron.bees.remove(curr_bee)
            curr_cron.bees.append(new_bee)

        db.commit()
