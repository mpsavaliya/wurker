import os
import glob
import copy
import shutil

import settings
from ..db import schema_exists, drop_schema
from models.wurker import Base


def run(args):
    """Inject Wurker dependency tree(s)."""

    if hasattr(args, "unsting") and args.unsting:
        return _delete_deps(args)

    if hasattr(args, "extract_wurker") and args.extract_wurker:
        return _extract_deps(args, args.extract_wurker, "wurker")

    if hasattr(args, "extract_colony") and args.extract_colony:
        return _extract_deps(args, args.extract_colony, "colony")

    default_deps = None
    default_root = os.path.join(args.app_root, "etc", "stingers")
    if default_root != args.root:
        args.root = os.path.abspath(args.root)
        default_args = copy.deepcopy(args)
        default_args.root = default_root
        default_args.slug = "default"
        default_args.node = None
        default_args.env = None
        default_deps = _get_deps(default_args)

    deps = _get_deps(args, deps=default_deps)
    _inject_deps(args, deps)

    if not os.path.exists(os.path.join(args.app_root, ".env")):
        return "ERROR: File not found: '.env'"


def _get_deps(args, deps=None):
    """Return a dependency tree for the given Stingers root and args."""

    if not os.path.exists(args.root):
        return f"Stingers root not found: '{args.root}'"

    stinger_dir = os.path.join(args.root, args.slug)
    if not os.path.exists(stinger_dir):
        return f"Stinger '{args.slug}' not found: '{stinger_dir}'"

    wurker_dir = os.path.join(stinger_dir, "wurker")
    colony_dir = os.path.join(stinger_dir, "colony")

    deps = deps or {
        "requirements.txt": [],
        ".env": [],
        "seeds": [],
        "models": [],
        "settings": [],
        "bees": [],
        "lib": [],
        "lib/mail": [],
        "ssl": [],
        "data": [],
    }

    print("==> Processing dependency directories...")

    # Do Colony 1st... it overrides Wurker
    node_dir = None
    if os.path.exists(colony_dir):
        node_dir = os.path.join(colony_dir, args.node) if args.node else None
        if node_dir and os.path.exists(node_dir):
            env_dir = os.path.join(node_dir, args.env) if args.env else None
            if env_dir and os.path.exists(env_dir):
                _load_deps(env_dir, deps)

            # Nodes may have wurker dirs, also
            node_wurker_dir = os.path.join(node_dir, "wurker")
            if os.path.exists(node_wurker_dir):
                _load_deps(node_wurker_dir, deps)

    # Now do Wurker...
    if os.path.exists(wurker_dir):
        _load_deps(wurker_dir, deps)

    return deps


def _load_deps(dep_dir:str, deps:dict):
    """Load dependency paths into provided dict of lists."""

    if not deps:
        return

    print(f"  . Processing dependency directory: '{dep_dir}'...")

    for dep in [d for d,p in deps.items()]:
        path = os.path.join(dep_dir, dep)
        if os.path.exists(path):
            if os.path.isfile(path) or os.listdir(path):
                deps[dep].append(os.path.join(dep_dir, dep))


def _inject_deps(args, deps):
    """Now inject deps."""

    if not deps:
        return

    print(f"==> Injecting dependencies...")

    dst_roots = {
        "requirements.txt": os.path.join(args.app_root, "etc"),
        ".env": args.app_root,
        "seeds": os.path.join(args.app_root, "etc"),
        "models": args.app_root,
        "settings": args.app_root,
        "bees": args.app_root,
        "lib": args.app_root,
        "lib/mail": args.app_root,
        "ssl": os.path.join(args.app_root, "etc", "nginx"),
        "data": args.app_root,
    }

    for dep, src_list in deps.items():
        if not src_list:
            continue

        dst = os.path.join(dst_roots[dep], dep)

        # Single file? Should only be one if Stingers set up correctly, but we pop, anyway
        if dep in ["requirements.txt", ".env",] and len(src_list):
            src = src_list.pop()

            # .env is special
            if dep == ".env" and os.path.exists(dst):
                print(f"  . Skipping existing '.env'")
                continue

            if os.path.isfile(src):
                print(f"  . Injecting: '{src}' -> '{dst}'")
                shutil.copyfile(src, dst)

            continue

        # Directories? Walk them and copy over missing dirs & files
        for src in src_list:
            for src_dir, dirs, files in os.walk(src):
                dst_dir = src_dir.replace(src, dst, 1)

                if not os.path.exists(dst_dir):
                    print(f"  . Creating directory: '{dst_dir}'")
                    os.makedirs(dst_dir)

                for f in files:
                    src_file = os.path.join(src_dir, f)
                    dst_file = os.path.join(dst_dir, f)
                    if os.path.exists(dst_file):
                        # Seed 000 should always get overwritten
                        if os.path.basename(dst_file) == "seed_data_000.json":
                            print(f"  . Overwriting existing '{dst_file}'")
                            os.remove(dst_file)
                        else:
                            print(f"  . Skipping existing '{dst_file}'")
                            continue

                    print(f"  . Injecting: '{src_file}' -> '{dst_dir}'")
                    shutil.copy(src_file, dst_dir)


def _delete_deps(args):
    """Unseed and delete all the injected dependencies."""

    # First we drop schema
    db_conf = settings.env.RemoteDB

    app_id = settings.env.app_id
    if not app_id:
        return "Missing valid WURKER_APP_ID"

    if args.drop_schema and schema_exists(db_conf, Base, return_bool=True):
        print(f"==> Dropping schema '{db_conf.database}' on host: '{db_conf.hostname}'...")
        if not drop_schema(db_conf, Base):
            return f"Unable to drop schema '{db_conf.database}' on host: '{db_conf.hostname}'"

    dst_roots = {
        "app.db": os.path.join(args.app_root, "data"),
        "hive.db": os.path.join(args.app_root, "data"),
        "requirements.txt": os.path.join(args.app_root, "etc"),
        ".env": args.app_root,
        "seeds": os.path.join(args.app_root, "etc"),
        "models": args.app_root,
        "settings": args.app_root,
        "bees": args.app_root,
        "lib": args.app_root,
        "lib/mail": args.app_root,
        "ssl": os.path.join(args.app_root, "etc", "nginx"),
        "data": args.app_root,
    }

    # Some files to NOT delete
    ignore = [
        "__init__.py", "wurker.py", "dispatch.py", "test_pybee.py", "dispatchers", "__pycache__",
        "base.py", "templates", "example.html", "example.txt",
    ]

    print(f"==> Deleting dependencies...")

    for dep, dst_root in dst_roots.items():
        dst = os.path.join(dst_root, dep)

        # Single file? Then just delete it
        if dep in ["app.db", "hive.db", "requirements.txt", ".env"]:
            if os.path.exists(dst):
                print(f"  . Deleting file: '{dst}'")
                os.remove(dst)

            continue

        # Directories? Then it really depends
        if dep == "seeds" and os.path.exists(dst):
            print(f"  . Deleting seeds: '{dst}'")
            shutil.rmtree(dst)

        elif dep == "models":
            for model in [f for f in glob.glob(os.path.join(dst, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Deleting model: '{model}'")
                os.remove(model)

        elif dep == "settings":
            for setting in [f for f in glob.glob(os.path.join(dst, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Deleting setting: '{setting}'")
                os.remove(setting)

            dst = os.path.join(dst, "bees")
            for setting in [f for f in glob.glob(os.path.join(dst, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Deleting bee setting: '{setting}'")
                os.remove(setting)

        elif dep == "bees":
            for bee in [f for f in glob.glob(os.path.join(dst, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Deleting bee: '{bee}'")
                os.remove(bee)

            # Also delete any custom submodules, except dispatchers/
            dirs = [
                f for f in glob.glob(os.path.join(dst, "*"))
                if os.path.isdir(f) and os.path.basename(f) not in ignore
            ]
            for d in dirs:
                print(f"  . Deleting bee directory: '{d}'")
                shutil.rmtree(d)

            dst = os.path.join(dst, "dispatchers")
            for cb in [f for f in glob.glob(os.path.join(dst, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Deleting dispatcher bee: '{cb}'")
                os.remove(cb)

        elif dep == "lib":
            for lib in [f for f in glob.glob(os.path.join(dst, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Deleting lib: '{lib}'")
                os.remove(lib)

        elif dep == "lib/mail":
            for item in [f for f in glob.glob(os.path.join(dst, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Deleting mail client: '{item}'")
                os.remove(item)

            # Also delete injected templates
            dst = os.path.join(dst, "templates")
            for item in [f for f in glob.glob(os.path.join(dst, "*")) if os.path.basename(f) not in ignore]:
                if os.path.isdir(item):
                    print(f"  . Deleting mail template directory: '{item}'")
                    shutil.rmtree(item)
                else:
                    print(f"  . Deleting mail template: '{item}'")
                    os.remove(item)

        elif dep == "data":
            for data in [f for f in glob.glob(os.path.join(dst, "*")) if os.path.basename(f) not in ignore]:
                if os.path.isdir(data):
                    print(f"  . Deleting data directory: '{data}'")
                    shutil.rmtree(data)
                else:
                    print(f"  . Deleting data file: '{data}'")
                    os.remove(data)


def _extract_deps(args, deps, target):
    """Extract (copy) a Stinger out to target directory ('wurker' or 'colony')."""

    if not args.root:
        return "Missing required --extract-root path"

    args.root = os.path.abspath(args.root)

    src_roots = {
        "requirements.txt": os.path.join(args.app_root, "etc"),
        ".env": args.app_root,
        "seeds": os.path.join(args.app_root, "etc"),
        "models": args.app_root,
        "settings": args.app_root,
        "settings/bees": args.app_root,
        "bees": args.app_root,
        "lib": args.app_root,
        "lib/mail": args.app_root,
        "ssl": os.path.join(args.app_root, "etc", "nginx"),
        "data": args.app_root,
    }

    # Build up the destination roots based on args and target
    target_root = os.path.join(args.root, args.slug, target)
    if target == "colony":
        target_root = os.path.join(target_root, args.node, args.env)

    # All deps?
    if len(deps) == 1 and deps[0] == "all":
        deps = list(src_roots.keys())

    dst_roots = {}
    for dep in src_roots:
        if not deps or dep in deps:
            dst_roots[dep] = target_root

    # Some files to NOT extract
    ignore = [
        "__init__.py", "wurker.py", "dispatch.py", "test_pybee.py", "seed_data_000.json", "app.db", "hive.db",
        "base.py", "example.html", "example.txt",
    ]

    for dep, dst_root in dst_roots.items():
        if not os.path.exists(dst_root):
            os.makedirs(dst_root, exist_ok=True)

        src = os.path.join(src_roots[dep], dep)
        dst = os.path.join(dst_root, dep)

        # Single file? Then just copy it
        if dep in ["requirements.txt", ".env"]:
            if not os.path.exists(src):
                print(f"  . File not found: '{src}'")
                continue

            if os.path.exists(dst):
                print(f"  . Overwriting file: '{dst}'")
                os.remove(dst)

            print(f"  . Extracting file: '{src}' -> '{dst}'")
            shutil.copy(src, dst)

            continue

        # Directories? Then it really depends
        if not os.path.exists(dst):
            os.makedirs(dst, exist_ok=True)

        if not os.path.exists(src):
            print(f"  . Directory not found: '{src}'")
            continue

        if dep == "seeds":
            for seed in [f for f in glob.glob(os.path.join(src, "*.json")) if os.path.basename(f) not in ignore]:
                print(f"  . Extracting seed: '{seed}' -> '{dst}'")
                shutil.copy(seed, dst)

        elif dep == "models":
            for model in [f for f in glob.glob(os.path.join(src, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Extracting model: '{model}' -> '{dst}'")
                shutil.copy(model, dst)

        elif dep == "settings":
            for setting in [f for f in glob.glob(os.path.join(src, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Extracting setting: '{setting}' -> '{dst}'")
                shutil.copy(setting, dst)

        elif dep == "settings/bees":
            for setting in [f for f in glob.glob(os.path.join(src, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Extracting bee setting: '{setting}' -> '{dst}'")
                shutil.copy(setting, dst)

        elif dep == "bees":
            for bee in [f for f in glob.glob(os.path.join(src, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Extracting bee: '{bee}' -> '{dst}'")
                shutil.copy(bee, dst)

            src = os.path.join(src, "dispatchers")
            dst = os.path.join(dst, "dispatchers")
            if not os.path.exists(dst):
                os.makedirs(dst, exist_ok=True)

            for cb in [f for f in glob.glob(os.path.join(src, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Extracting dispatcher bee: '{cb}' -> '{dst}'")
                shutil.copy(cb, dst)

        elif dep == "lib":
            for lib in [f for f in glob.glob(os.path.join(src, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Extracting lib: '{lib}' -> '{dst}'")
                shutil.copy(lib, dst)

        elif dep == "lib/mail":
            for item in [f for f in glob.glob(os.path.join(src, "*.py")) if os.path.basename(f) not in ignore]:
                print(f"  . Extracting mail client: '{item}' -> '{dst}'")
                shutil.copy(item, dst)

            src = os.path.join(src, "templates")
            dst = os.path.join(dst, "templates")
            if not os.path.exists(dst):
                os.makedirs(dst, exist_ok=True)

            for item in [f for f in glob.glob(os.path.join(src, "*")) if os.path.basename(f) not in ignore]:
                print(f"  . Extracting mail template: '{item}' -> '{dst}'")

                if os.path.isdir(item):
                    dst = os.path.join(dst, os.path.basename(item))
                    shutil.copytree(item, dst, dirs_exist_ok=True)
                else:
                    shutil.copy(item, dst)

        elif dep == "data":
            for data in [f for f in glob.glob(os.path.join(src, "*")) if os.path.basename(f) not in ignore]:
                print(f"  . Extracting data: '{data}' -> '{dst}'")
                shutil.copy(data, dst)
