import os
import re
import json

import settings
from wurker.util import BotArgs
from models.wurker import Base, LocalBase, User, ApiAuth
from wurker.subcommands.status import run as runstatus
from ..db import create_schema, drop_schema, seed_data, seed_control, get_control, make_session, update_status

from app import create_app
from app.db import init_db
from werkzeug.security import generate_password_hash


def run(args):
    """Seed schema and data for this Wurker instance. Note NO Wurker given!"""

    env = settings.env.env or "production"

    single_action = any([
        args.schema_only, args.control_only, args.data_only, args.local_only,
        args.drop_only, args.schema_drop_only, args.local_drop_only,
    ])

    if not single_action or args.schema_only or args.drop_only or args.schema_drop_only:
        error = _seed_schema(args, env)
        if error:
            return error

    if not single_action or args.control_only:
        error = _seed_control(args, env)
        if error:
            return error

    if not single_action or args.data_only:
        error = _seed_data(args, env)
        if error:
            return error

        # We seed the wurker_status data now, too
        error = _seed_status(args, env)
        if error:
            return error

    if not single_action or args.local_only or args.drop_only or args.local_drop_only:
        error = _seed_local(args, env)
        if error:
            return error

        error = _seed_app(args, env)
        if error:
            return error


def _seed_schema(args, env):
    """Seed schema (or drop it)."""

    db_conf = settings.env.RemoteDB

    # Validate we have an app (control) ID
    app_id = settings.env.app_id
    if not app_id:
        return "Missing valid WURKER_APP_ID"

    # Drop the schema?
    if args.drop_only or args.schema_drop_only:
        print(f"==> Dropping schema '{db_conf.database}' on host: '{db_conf.hostname}'...")
        if not drop_schema(db_conf, Base):
            return f"Unable to drop schema '{db_conf.database}' on host: '{db_conf.hostname}'"
        return

    print(f"==> Creating schema if it does not exist: '{db_conf.database}'...")

    # It won't do anything if there's nothing to do
    if not create_schema(db_conf, Base):
        return f"Unable to create schema '{db_conf.database}' on host: '{db_conf.hostname}'"

    print(f"  . Schema exists or was created: '{db_conf.database}'")


def _seed_control(args, env):
    """Seed control for this Wurker instance."""

    db_conf = settings.env.RemoteDB

    print(f"==> Seeding 'wurker_control' on host: '{db_conf.hostname}'...")

    # Validate we have an app (control) ID
    app_id = settings.env.app_id
    if not app_id:
        return "Missing valid WURKER_APP_ID"

    # Make the control record (TODO: make this distributable)
    if not seed_control(db_conf, app_id):
        return f"Unable to seed 'wurker_control' record on host: '{db_conf.hostname}'"

    print(f"  . Seeded 'wurker_control' for app ID ({app_id})")


def _seed_data(args, env):
    """Seed config data."""

    db_conf = settings.env.RemoteDB

    print(f"==> Seeding data on host: '{db_conf.hostname}'...")

    # Validate we have an app (control) ID
    app_id = settings.env.app_id
    if not app_id:
        return "Missing valid WURKER_APP_ID"

    for data_path in [os.path.abspath(p) for p in args.data_paths]:
        if not os.path.exists(data_path):
            return f"Seed file not found: '{data_path}'"

        seed, error, warnings = seed_data(db_conf, app_id, data_path, args.app_root)
        if error:
            return f"Error seeding ({seed}) from '{data_path}': {error}"
        elif seed == None:
            print(f"  . Already seeded: '{data_path}'")
        else:
            msg = f"  . Seeded ({seed}): '{data_path}'"
            if warnings:
                msg += " with warnings:\n"
                for s, warning in warnings:
                    print(f"    ! WARNING: Seed ({s}): {warning}")

    print("  . All data seeded")


def _seed_status(args, env):
    """Seed wurker_status from local settings and derived values."""

    db_conf = settings.env.RemoteDB

    print(f"==> Seeding 'wurker_status' on host: '{db_conf.hostname}'...")

    # Validate we have an app (control) ID
    app_id = settings.env.app_id
    if not app_id:
        return "Missing valid WURKER_APP_ID"

    args = BotArgs(app_root=settings.env.app_root)
    args.return_result = True
    status = runstatus(args)

    # This function raises an Exception per missing required key
    try:
        update_status(db_conf, app_id, status)
    except Exception as ex:
       return f"Unable to seed 'wurker_status' record on host '{db_conf.hostname}' due to exception: {ex}"

    print(f"  . Seeded 'wurker_status' for app ID ({app_id})")


def _seed_local(args, env):
    """Seed local hive DB."""

    db_conf = settings.env.LocalDB
    db_file = os.path.join(args.app_root, db_conf.database)

    if args.drop_only or args.local_drop_only:
        print(f"==> Deleting local database file if it exists: '{db_file}'...")
        if os.path.exists(db_file):
            os.remove(db_file)
        return

    print(f"==> Creating local database file if it does not exist: '{db_file}'...")

    if not os.path.exists(os.path.dirname(db_file)):
        os.makedirs(os.path.dirname(db_file))

    if not create_schema(db_conf, LocalBase):
        return f"Unable to create local database file: '{db_file}'"

    print(f"  . Local database file exists or was created: '{db_file}'")


def _seed_app(args, env):
    """Seed Flask App DB."""

    db_conf = settings.env.AppDB
    db_file = os.path.join(args.app_root, db_conf.database)

    if args.drop_only or args.local_drop_only:
        print(f"==> Deleting app database file if it exists: '{db_file}'...")
        if os.path.exists(db_file):
            os.remove(db_file)
        return

    print(f"==> Creating app database file if it does not exist: '{db_file}'...")

    db_dir = os.path.dirname(db_file)
    if not os.path.exists(db_dir):
        os.makedirs(db_dir)

    # Note we tell Flask app to create its own DB
    try:
        app = create_app()
        with app.app_context():
            init_db()
    except Exception as ex:
        return f"Exception encountered during app database file '{db_file}' creation: {ex}"

    print(f"  . App database file exists or was created: '{db_file}'")

    # Seed SQLite table data
    session = make_session(db_conf=db_conf)

    print(f"==> Seeding 'user' in app database file: '{db_file}'...")
    _seed_auth(db_dir, 'user', User, session)

    print(f"==> Seeding 'api_auth' in app database file: '{db_file}'...")
    _seed_auth(db_dir, 'api_auth', ApiAuth, session)


def _seed_auth(db_dir, table, model, session):
    """Seed Wurker auth."""

    # If data/user.json exists, then seed the app.db from it
    users = None
    user_file = os.path.join(db_dir, f"{table}.json")
    if os.path.exists(user_file):
        with open(user_file, 'r') as f:
            users = json.load(f)

    if not users:
        return

    with session() as db:
        for user_cfg in users:
            if "username" not in user_cfg or "password" not in user_cfg:
                print(f"  . Skipping invalid '{table}.json' data: {user_cfg}")
                continue

            user_cfg["password"] = generate_password_hash(user_cfg["password"])
            user = model(**user_cfg)
            db.add(user)
            db.commit()

            print(f"  . Created app user ({user.id}): '{user.username}'")
