import os
import sys
import argparse
import datetime

import settings
from ..db import schema_exists
from models.wurker import Base
from . import sting, seed, status, show, run, kill, add, edit, remove, control, service, test, report


def add_subparsers(parser, app_root, stingers_root=None, seed_dir=None):
    """Tack the command subparsers onto the top parser."""

    dotenv_file = os.path.join(app_root, ".env")

    # SUBCOMMANDS subparser
    subcommands = parser.add_subparsers(dest="subcommand")

    # Check that Wurker schema exists in target DB
    def db_ready():
        """Assembles the pieces to do basic pre-run check that the config DB is G2GB."""

        # Gotta have an .env file for DB connection, so...
        if not os.path.exists(dotenv_file):
            return False

        app_env = settings.env.env or "production"
        db_conf = settings.env.RemoteDB

        if not db_conf.dbdriver:
            return False

        print(f"WARNING: Checking database '{db_conf.hostname}'. Press Ctrl-C to cancel ...", file=sys.stderr)
        try:
            schema_exists(db_conf, Base, connect_args={'connect_timeout': 10})
        except Exception as ex:
            print(f"WARNING: {ex}", file=sys.stderr)
            return False

        return True

    try:
        db_ready = db_ready()
    except KeyboardInterrupt:
        raise Exception("Interrupted by user!")

    # Sting command can inject structured dependencies, including seeds
    sting_cmd = subcommands.add_parser(
        "sting", help="Inject Wurker configuration dependencies",
        description="Inject Wurker configuration dependencies"
    )
    sting_cmd.set_defaults(func=sting.run)

    if not db_ready and not os.path.exists(".env"):
        sting_cmd.add_argument(
            "--root", default=stingers_root, help=f"Path to root of Stingers tree (default: '{stingers_root}')"
        )
        sting_cmd.add_argument("--slug", default='default', help="Stinger slug (default: 'default')")
        sting_cmd.add_argument(
            "--node", default='queen',
            help="Colony node slug, like: 'queen' or 'drone1' (default: 'queen')"
        )
        sting_cmd.add_argument(
            "--env", default='development',
            help="Environment slug, like: 'production' or 'development' (default: 'development')"
        )
    else:
        deps = [
            "all", "requirements.txt", ".env", "seeds", "models", "bees", "settings", "settings/bees",
            "lib", "lib/mail", "ssl", "data",
        ]
        app_env = settings.env.env or "production"

        sting_meg = sting_cmd.add_mutually_exclusive_group()
        sting_meg.add_argument(
            "--unsting", action="store_true", help="Unseed and delete previously injected dependencies"
        )
        sting_meg.add_argument(
            "--extract-wurker", nargs="*", metavar="DEP",
            help="Extract dependencies to 'wurker' tree (choices: {0})".format(deps)
        )
        sting_meg.add_argument(
            "--extract-colony", nargs="*", metavar="DEP",
            help="Extract dependencies to 'colony' tree (choices: {0})".format(deps)
        )
        sting_cmd.add_argument("--drop-schema", action="store_true", help="Also drop DB schema (unsting only)")
        sting_cmd.add_argument("--root", default=stingers_root, help="Path to root of Stingers tree (extract only)")
        sting_cmd.add_argument("--slug", default='default', help="Stinger slug (extract only, default: 'default')")
        sting_cmd.add_argument("--node", default='queen', help="Colony node slug (extract only, default: 'queen')")
        sting_cmd.add_argument(
            "--env", default=app_env, help="Environment slug (extract only, default: '{0}')".format(app_env)
        )

    # Disable the 'seed' command by emptying the .env to 'WURKER_APP_SEED_DIR='
    if seed_dir and os.path.exists(dotenv_file):
        seed_cmd = subcommands.add_parser(
            "seed", help="Install and seed Wurker data resource(s)",
            description="Install and seed Wurker data resource(s)"
        )

        seed_meg = seed_cmd.add_mutually_exclusive_group()
        seed_meg.add_argument(
            "--schema-only", action="store_true", help="Only create schema in config DB"
        )
        seed_meg.add_argument(
            "--control-only", action="store_true", help="Only insert control data in config schema"
        )
        seed_meg.add_argument(
            "--data-only", action="store_true", help="Only insert data in config schema"
        )
        seed_meg.add_argument(
            "--local-only", action="store_true", help="Only create local hive DB"
        )
        seed_meg.add_argument(
            "--drop-only", action="store_true", help="Only drop schema and local hive DBs"
        )
        seed_meg.add_argument(
            "--schema-drop-only", action="store_true", help="Only drop schema from config DB"
        )
        seed_meg.add_argument(
            "--local-drop-only", action="store_true", help="Only drop local hive DB"
        )

        seed_cmd.set_defaults(func=seed.run)

    # We want to group these subcommands with other non-API ones
    if db_ready:

        # test subcommand
        test_cmd = subcommands.add_parser(
            "test", help="Run unit tests (stop services first or it may fail)",
            description="Run unit tests (stop services first or it may fail)"
        )
        test_cmd.add_argument("-v", "--verbose", action="store_true", help="Run tests with verbosity")
        test_cmd.set_defaults(func=test.run)

        # service subcommand
        virtualenv = os.path.dirname(os.path.dirname(sys.executable))
        socket = os.path.join("/tmp", "wurker-ui.sock")
        domain = (
            f"default: '{settings.env.server_domain}'" if settings.env.server_domain
            else f"example: 'queen.example.com'"
        )

        ssl_dir = os.path.join(app_root, 'etc', 'nginx', 'ssl')
        ssl_certs = ['wurker.crt', 'wurker.key']

        # TODO Move this to .env if it works
        workers = 3

        svc_cmd = subcommands.add_parser(
            "service", help="Configure and install systemd services",
            description="Configure and install systemd services"
        )
        svc_meg = svc_cmd.add_mutually_exclusive_group()
        svc_meg.add_argument("--wurker-only", action="store_true", help="Only configure 'wurker.service'")
        svc_meg.add_argument("--ui-only", action="store_true", help="Only configure 'wurker-ui.service'")
        svc_cmd.add_argument(
            "--virtualenv", help=f"Root path of the Python venv to use (default: '{virtualenv}')", default=virtualenv
        )
        svc_cmd.add_argument(
            "--workingdir", help=f"Parent path of the Wurker directory (default: '{app_root}')", default=app_root
        )
        svc_cmd.add_argument(
            "--domain", help=f"Domain name for 'wurker-ui' virtual host ({domain})",
            default=settings.env.server_domain
        )
        svc_cmd.add_argument(
            "--workers", help=f"Number of `wurker-ui` workers (default: {workers}')", default=workers
        )
        svc_cmd.add_argument(
            "--socket", help=f"Path to unix socket for 'wurker-ui' (default: '{socket}')", default=socket
        )
        svc_cmd.add_argument(
            "--ssl-dir", help=f"Path to Nginx SSL certs for 'wurker-ui' (default: '{ssl_dir}')", default=ssl_dir
        )
        svc_cmd.add_argument(
            "--ssl-certs", nargs=2, metavar=('CRT', 'KEY'), default=ssl_certs,
            help=f"Names of SSL certs for 'wurker-ui' (default: '{ssl_certs}')"
        )
        svc_cmd.add_argument(
            "--install", action="store_true", help="Also install the configured service(s)"
        )
        svc_cmd.set_defaults(func=service.run)

    # Status command is very basic
    if (db_ready or seed_dir) and os.path.exists(".env"):
        status_cmd = subcommands.add_parser(
            "status", help="Show Wurker status",
            description="Show Wurker status"
        )

        if settings.env.is_queen:
            status_grp = status_cmd.add_argument_group("colony options", "Request via Drone API(s)")
            status_grp.add_argument("--drone", nargs="*", help="One or more slugs or IDs of the Drone(s) to query")

        status_cmd.set_defaults(func=status.run)

    # If the DB isn't ready and seeded, all the fun stuff will be disabled
    if db_ready:

        # control subcommand
        control_cmd = subcommands.add_parser(
            "control", help="Control the Wurker Bee Hive",
            description="Control the Wurker Bee Hive"
        )
        control_cmd.add_argument("action", choices=["reload", "pause"], help="The action to perform")

        if settings.env.is_queen:
            control_grp = control_cmd.add_argument_group("colony options", "Request via Drone API(s)")
            control_grp.add_argument("--drone", nargs="*", help="One or more slugs or IDs of the Drone(s) to query")

        control_cmd.set_defaults(func=control.run)

        # show subcommand
        show_options = ["supervised", "scheduled", "ondemand", "hive", "command", "cron", "bee",]
        show_cmd = subcommands.add_parser(
            "show", help="Show Wurker data, with filters",
            description="Show Wurker data, with filters"
        )
        show_cmd.add_argument("--any", action="store_true", help="Show any matching Bees, including disabled")
        show_cmd.add_argument("--filter", dest="option", choices=show_options, help="Only show filtered item type")
        show_cmd.add_argument(
            "--slug", dest="key", nargs="?", const=True, help="Also filter by SLUG (only: command, cron, bee)"
        )
        show_cmd.add_argument("--widowed", action="store_true", help="Show only unattached (only: command, cron)")

        if settings.env.is_queen:
            show_grp = show_cmd.add_argument_group("colony options", "Request via Drone API(s)")
            show_grp.add_argument("--drone", nargs="*", help="One or more slugs or IDs of the Drone(s) to query")

        show_cmd.set_defaults(func=show.run, options=show_options)

        # add subcommand
        add_cmd = subcommands.add_parser(
            "add", help="Add one Bee, Cron, or Map",
            description="Add one Bee, Cron, or Map"
        )
        add_cmd.add_argument(
            "option", choices=["bee", "cron", "map", "command", "ondemand"],
            help="What type of item to add"
        )
        add_cmd.add_argument(
            "json", nargs='?', type=argparse.FileType('r'), default=sys.stdin,
            help="Optional stdin JSON file (<) or heredoc (<<EOF...EOF)"
        )

        if settings.env.is_queen:
            add_grp = add_cmd.add_argument_group("colony options", "Request via Drone API(s)")
            add_grp.add_argument("--drone", nargs="*", help="One or more slugs or IDs of the Drone(s) to query")

        add_cmd.set_defaults(func=add.run)

        # edit subcommand
        edit_cmd = subcommands.add_parser(
            "edit", help="Edit one Bee, Cron, or Map",
            description="Edit one Bee, Cron, or Map"
        )
        edit_cmd.add_argument(
            "option", choices=["bee", "cron", "map", "command"],
            help="What type of item to edit"
        )
        edit_cmd.add_argument("key", nargs='+', help="Slug (bee, cron, command) or PK (map) used to select record")
        edit_cmd.add_argument(
            "json", nargs='?', type=argparse.FileType('r'), default=sys.stdin,
            help="Optional stdin JSON file (<) or heredoc (<<EOF...EOF)"
        )

        if settings.env.is_queen:
            edit_grp = edit_cmd.add_argument_group("colony options", "Request via Drone API(s)")
            edit_grp.add_argument("--drone", nargs="*", help="One or more slugs or IDs of the Drone(s) to query")

        edit_cmd.set_defaults(func=edit.run)

        # remove subcommand
        rm_cmd = subcommands.add_parser(
            "rm", help="Remove one Bee, Cron, or Map",
            description="Remove one Bee, Cron, or Map"
        )
        rm_cmd.add_argument(
            "option", choices=["bee", "cron", "map", "command", "ondemand"],
            help="What type of item to remove"
        )
        rm_cmd.add_argument("key", nargs='+', help="Slug (bee, cron, command) or PK (map) used to select record")

        if settings.env.is_queen:
            rm_grp = rm_cmd.add_argument_group("colony options", "Request via Drone API(s)")
            rm_grp.add_argument("--drone", nargs="*", help="One or more slugs or IDs of the Drone(s) to query")

        rm_cmd.set_defaults(func=remove.run)

        # run subcommand
        run_options = ["supervised", "scheduled", "ondemand", "cron", "bee",]
        run_cmd = subcommands.add_parser(
            "run", help="Run one or more mapped Bees, with filters",
            description="Run one or more mapped Bees, with filters"
        )
        run_cmd.add_argument("--filter", dest="option", choices=run_options, help="Only run filtered item type")
        run_cmd.add_argument(
            "--slug", dest="key", nargs="?", const=True, help="Also filter by SLUG (only: cron, bee)"
        )

        run_cmd.set_defaults(func=run.run, options=run_options)

        # kill subcommand
        kill_options = ["supervised", "scheduled", "ondemand", "hive", "cron", "bee",]
        kill_cmd = subcommands.add_parser(
            "kill", help="Kill one or more Bees, with filters",
            description="Kill one or more Bees, with filters"
        )
        kill_cmd.add_argument("--filter", dest="option", choices=kill_options, help="Only kill filtered item type")
        kill_cmd.add_argument(
            "--slug", dest="key", nargs="?", const=True, help="Also filter by SLUG (only: cron, bee)"
        )

        if settings.env.is_queen:
            kill_grp = kill_cmd.add_argument_group("colony options", "Request via Drone API(s)")
            kill_grp.add_argument("--drone", nargs="*", help="One or more slugs or IDs of the Drone(s) to query")

        kill_cmd.set_defaults(func=kill.run, options=kill_options)

        # report subcommand
        report_options = ["bee", "cron"]
        report_columns = [
            'id', 'bee_id', 'bee_slug', 'bee_name', 'cron_id', 'cron_slug', 'cron_name',
            'hive_pid', 'hive_timestamp', 'hive_runcount', 'hive_duration', 'status',
            'created_at', 'updated_at',
        ]

        report_cmd = subcommands.add_parser(
            "report",
            help="View Wurker Bee run reports",
            description="View Wurker Bee run reports"
        )
        report_cmd.add_argument("--filter", dest="option", choices=report_options, help="Only view filtered report")
        report_cmd.add_argument(
            "--slug", dest="key", nargs="?", const=True, help="Also filter by SLUG (only: cron, bee)"
        )
        report_cmd.add_argument("--start", help="Optional start date/time")
        report_cmd.add_argument("--end", help="Optional end date/time")
        report_cmd.add_argument("--page", type=int, default=0, help="Pagination page number (default: 0)")
        report_cmd.add_argument("--page-limit", type=int, default=20, help="Records per page (default: 20)")
        report_cmd.add_argument("--sort", choices=report_columns, default='id', help="Sort by column (default: id)")
        report_cmd.add_argument(
            "--sort-order", choices=["asc", "desc"], default="asc", help="Sort direction (default: asc)"
        )

        if settings.env.is_queen:
            report_grp = report_cmd.add_argument_group("colony options", "Request via Drone API(s)")
            report_grp.add_argument("--drone", nargs="*", help="One or more slugs or IDs of the Drone(s) to query")

        report_cmd.set_defaults(func=report.run, options=report_options)
