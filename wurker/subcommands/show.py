import sys
import json
import logging
import datetime

import settings
from ..db import get_drone_status
from ..drone import make_drone_clients
from ..util import DateTimeEncoder, log
from models.wurker import WurkerBee, WurkerCron, WurkerCommand


def mk_bee_cfg(bee, cron=None):
    """Returns a Bee config dict (for now)."""

    cfg = {
        "id": bee.id,
        "app_id": str(bee.app_id),
        "name": bee.name,
        "slug": bee.slug,
        "description": bee.description,
        "command_id": bee.command_id,
        "command_slug": bee.command.slug,
        "args": bee.args,
        "quiet_restart": bee.quiet_restart,
        "enabled": bee.enabled,
        "module": None,
        "command": None,
    }

    cmd_key = "module" if bee.command.is_module else "command"
    cfg[cmd_key] = bee.command.command

    if cron is not None:
        cfg["cron_id"] = cron.id
        cfg["cron"] = cron.cron

    return cfg


def mk_cron_cfg(cron):
    """Returns a Cron config dict (for now)."""

    return {
        "id": cron.id,
        "app_id": str(cron.app_id),
        "name": cron.name,
        "slug": cron.slug,
        "cron": cron.cron,
        "overrun": cron.overrun,
    }


def mk_command_cfg(command):
    """Returns a Command config dict (for now)."""

    return {
        "id": command.id,
        "app_id": str(command.app_id),
        "name": command.name,
        "slug": command.slug,
        "description": command.description,
        "command": command.command,
        "is_module": command.is_module,
    }


def run_drone(args):
    """Run via Drone API(s)."""

    db_conf = settings.env.RemoteDB
    app_id = settings.env.app_id
    drone_status = get_drone_status(db_conf, app_id, drones=args.drone)
    if drone_status is None or not len(drone_status):
        print(f"WARNING: Queen ({app_id}) Drones have no 'wurker_status' data", file=sys.stderr)
        return

    drone_clients = make_drone_clients(drone_status, auth=settings.env.ApiAuth)

    results = {}
    for app_id, drone in drone_clients.items():
        result = drone.get_show(option=args.option, key=args.key)

        if result is None:
            print(f"WARNING: No 'show' result from Drone '{drone.slug}' ({drone.app_id})", file=sys.stderr)
            continue

        results[app_id] = result

    if results is None:
        print(f"WARNING: No 'show' results from Drones", file=sys.stderr)
        return

    if hasattr(args, "return_result") and args.return_result:
        return results

    # OK, everything else would've printed to stderr... this is the final successful output
    print(json.dumps(results, sort_keys=True, indent=2))


def run(wurkman):
    """Show Wurker Bees."""

    # Run via Drone API?
    if hasattr(wurkman.args, "drone") and wurkman.args.drone is not None:
        return run_drone(wurkman.args)

    with wurkman.session() as db:

        # If --command, --cron, or --bee set with no args
        if wurkman.args.command:
            query = db.query(WurkerCommand).filter(WurkerCommand.app_id == wurkman.app_id)

            if isinstance(wurkman.args.command, str):
                query = query.filter(WurkerCommand.slug == wurkman.args.command)

            commands = []
            for command in query.all():
                if wurkman.args.widowed and len(command.bees):
                    continue
                commands.append(mk_command_cfg(command))

            if hasattr(wurkman.args, "return_result") and wurkman.args.return_result:
                return commands

            print(json.dumps(commands, indent=2, cls=DateTimeEncoder))
            return

        if wurkman.args.cron:
            query = db.query(WurkerCron).filter(WurkerCron.app_id == wurkman.app_id)
            if isinstance(wurkman.args.cron, str):
                query = query.filter(WurkerCron.slug == wurkman.args.cron)

            crons = []
            for cron in query.all():
                if wurkman.args.widowed and len(cron.bees):
                    continue
                crons.append(mk_cron_cfg(cron))

            if hasattr(wurkman.args, "return_result") and wurkman.args.return_result:
                return crons

            print(json.dumps(crons, indent=2, cls=DateTimeEncoder))
            return

        if wurkman.args.bee == True or (isinstance(wurkman.args.bee, str) and wurkman.args.any):
            query = db.query(WurkerBee).filter(WurkerBee.app_id == wurkman.app_id)
            if wurkman.args.bee != True:
                query = query.filter(WurkerBee.slug == wurkman.args.bee)

            bees = []
            for bee in query.all():
                bees.append(mk_bee_cfg(bee))

            if hasattr(wurkman.args, "return_result") and wurkman.args.return_result:
                return bees

            print(json.dumps(bees, indent=2, cls=DateTimeEncoder))
            return

        # OK we're displaying attached bees then
        bees, bee_ct = wurkman.get_bees()

        if wurkman.args.supervised:
            bees = bees["supervised"]
        elif wurkman.args.scheduled:
            bees = bees["scheduled"]
        elif wurkman.args.ondemand:
            bees = bees["ondemand"]
        elif wurkman.args.hive:
            bees = wurkman.get_hive(incl_duration=True)

        if hasattr(wurkman.args, "return_result") and wurkman.args.return_result:
            return bees

        print(json.dumps(bees, sort_keys=True, indent=2, cls=DateTimeEncoder))
