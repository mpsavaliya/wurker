import os
import time
import json
import logging
import traceback

from datetime import datetime, timedelta

import settings
from wurker import Session
from wurker.hive import Hive
from wurker.base import Bee, PyBee
from wurker.db import update_status
from wurker.util import log, BotArgs
from wurker.subcommands.status import run as runstatus
from models.wurker import WurkerBeeState, WurkerControl, WurkerBee, WurkerCron


# Using globals, so let's be up-front... here they are.
class Globals:
    STOP = False
    PAUSE = False


class Wurkman:
    """The controller class."""

    app_id = None

    args = None
    loglevel = None

    db_conf = None
    session = None

    bee_ct = 0
    slug_map = None

    control = None
    hive = None
    bees = None

    def __init__(self, args, session=None):
        self.args = args

        self.app_id = settings.env.app_id
        self.loglevel = args.loglevel or logging.INFO

        self._init_db(session=session)
        self._init_slug_map()
        self._init_status()

    def _init_db(self, session=None):
        """Initialize the configuration and local DB connections."""

        self.db_conf = settings.env.RemoteDB
        log(f"Connecting to config DB: {self.db_conf.hostname}...")
        self.session = session or Session

        # And then load the hive
        self.hive = Hive(app_root=self.args.app_root)

    def _init_slug_map(self):
        """Initialize ID-slug maps for crons and bees. Used by hive logic."""

        slug_map = {"bee":{}, "cron":{None:"ondemand"}}

        with self.session() as db:
            for bee in db.query(WurkerBee).filter(WurkerBee.app_id == self.app_id).all():
                slug_map["bee"][bee.id] = bee.slug

            for cron in db.query(WurkerCron).filter(WurkerCron.app_id == self.app_id).all():
                slug_map["cron"][cron.id] = cron.slug

        self.slug_map = slug_map

    def _init_status(self):
        """Initialize wurker_status table from local settings and state."""

        # Get the status data from the 'status' subcommand
        args = BotArgs(app_root=settings.env.app_root)
        args.return_result = True
        status = runstatus(args)

        # Use the util to update DB to validate 1st
        try:
            update_status(self.db_conf, self.app_id, status)
        except Exception as ex:
            raise Exception(
                f"Failed to update 'wurker_status' on host '{self.db_conf.hostname}' due to exception: {ex}"
            )

    @property
    def Now(self):
        return datetime.now()

    def rm_bee(self, wurk_type, cron, bee):
        """Delete a bee from self.bees (for run_once CLI logic)"""

        if not self.bees or wurk_type not in self.bees:
            return

        cron_slug = cron if isinstance(cron, str) else cron.slug
        bee_slug = bee if isinstance(bee, str) else bee.slug

        if cron_slug not in self.bees[wurk_type] or bee_slug not in self.bees[wurk_type][cron_slug]:
            return

        log(f"Removing Wurker Bee '{bee_slug}' from '{wurk_type}' cache", level=logging.DEBUG)
        del self.bees[wurk_type][cron_slug][bee_slug]

        # We also have to remove ondemand bees from the queue table
        if wurk_type == "ondemand":
            log(f"Removing Wurker Bee '{bee_slug}' from ondemand queue", level=logging.DEBUG)

            with self.session() as db:
                if isinstance(bee, str):
                    bee = db.query(WurkerBee).filter(
                        WurkerBee.slug == bee_slug,
                        WurkerBee.app_id == self.app_id
                    ).first()

                # No queue? Weird! But OK...
                if not len(bee.queue):
                    log(f"Wurker Bee '{bee_slug}' not queued!", level=logging.WARN)
                    return

                # Delete the bee from the queue
                queue = bee.queue[0]
                db.delete(queue)
                db.flush()
                db.commit()

    def get_bees(self, wurk_type=None):
        """
        Return scheduled Wurker Bees (jobs). Looks confusing, but basically, it boils down to:
        - Bee only: dict with [bee.slug] for keys
        - Cron only: dict with [cron.slug][bee.slug] for keys
        - Scheduled/Supervised:
        """

        if wurk_type and wurk_type not in ["scheduled", "supervised", "ondemand",]:
            raise Exception(f"Invalid wurk type: '{wurk_type}'")

        bee_ct = 0
        show_cmd = (self.args.subcommand == "show")

        log("Obtaining Wurker Bees from DB", level=logging.DEBUG)

        # Let's decide what 'bees' response should look like...
        bees = {"scheduled": {}, "supervised": {}, "ondemand": {}, "orphaned": {}, "disabled": {}}

        with self.session() as db:
            query = db.query(WurkerBee).filter(WurkerBee.app_id == self.app_id)
            if self.args.cron:
                query = query.filter(WurkerBee.crons.any(slug=self.args.cron))
                bees = {} if show_cmd else bees
            elif self.args.bee:
                query = query.filter(WurkerBee.slug == self.args.bee)
                bees = {} if show_cmd else bees

            result = query.all()
            for dbee in result:
                # Note we load ondemand separately and first
                if len(dbee.queue) and "ondemand" in bees:
                    if "ondemand" not in bees["ondemand"]:
                        bees["ondemand"]["ondemand"] = {}

                    bee = (
                        PyBee(self.session, dbee, args=dbee.queue[0].args) if dbee.command.is_module
                        else Bee(self.session, dbee, args=dbee.queue[0].args)
                    )
                    bees["ondemand"]["ondemand"][dbee.slug] = bee.AsDict if show_cmd else bee
                    bee_ct += 1

                    if not show_cmd and wurk_type == "ondemand":
                        continue

                # Bee has no crons? Use --any if you want orphaned bees and childless crons
                if not len(dbee.crons) and not self.args.ondemand:
                    if "orphaned" in bees:
                        if "orphaned" not in bees["orphaned"]:
                            bees["orphaned"]["orphaned"] = {}

                        bee = PyBee(self.session, dbee) if dbee.command.is_module else Bee(self.session, dbee)
                        bees["orphaned"]["orphaned"][dbee.slug] = bee.AsDict if show_cmd else bee

                    continue

                for cron in dbee.crons:
                    bee = PyBee(self.session, dbee, cron) if dbee.command.is_module else Bee(self.session, dbee, cron)

                    if not bee.enabled:
                        if "disabled" in bees:
                            if bee.cron_slug not in bees["disabled"]:
                                bees["disabled"][bee.cron_slug] = {}
                            bees["disabled"][bee.cron_slug][bee.slug] = bee.AsDict if show_cmd else bee

                        continue

                    # Just the 'show' command? Let's get 'er done...
                    if show_cmd and (self.args.bee or self.args.cron):
                        if self.args.bee:
                            bees[bee.slug] = bee.AsDict if show_cmd else bee

                        if self.args.cron:
                            if bee.cron_slug not in bees:
                                bees[bee.cron_slug] = {}
                            bees[bee.cron_slug][bee.slug] = bee.AsDict if show_cmd else bee

                        bee_ct += 1
                        continue

                    # Supervised?
                    if bee.cron is None:
                        if bee.cron_slug not in bees["supervised"]:
                            bees["supervised"][bee.cron_slug] = {}
                        bees["supervised"][bee.cron_slug][bee.slug] = bee.AsDict if show_cmd else bee
                        bee_ct += 1
                        continue

                    # Or Scheduled?
                    if bee.cron_slug not in bees["scheduled"]:
                        bees["scheduled"][bee.cron_slug] = {}
                    bees["scheduled"][bee.cron_slug][bee.slug] = bee.AsDict if show_cmd else bee
                    bee_ct += 1

        # Is wurk_type set? Then just return that
        if wurk_type:
            bee_ct = sum(len(b) for b in bees[wurk_type].values())
            return bees[wurk_type], bee_ct

        if hasattr(self.args, "any") and self.args.any:
            return bees, bee_ct

        # Not --any so only return scheduled Bees
        if "orphaned" in bees:
            del bees["orphaned"]
        if "disabled" in bees:
            del bees["disabled"]

        return bees, bee_ct

    def get_hive(self, incl_duration=True):
        """Get Hive data from local DB, formatted for display."""

        # For 'show' subcommand, we trick get_bees() to get real bees
        subcommand = self.args.subcommand
        self.args.subcommand = "hive"
        bees, bee_ct = self.get_bees()
        self.args.subcommand = subcommand

        flat = {}
        for section, crons in bees.items():
            for cron_slug, cron in crons.items():
                if cron_slug not in flat:
                    flat[cron_slug] = {}

                for bee_slug, bee in cron.items():
                    if bee_slug not in flat[cron_slug]:
                        flat[cron_slug][bee_slug] = bee

        hive = {}
        for cron_id, cron in self.hive.hive.items():
            cron_slug = "ondemand" if cron_id is None else self.slug_map["cron"][cron_id]
            if cron_slug not in flat:
                continue

            for bee_id, item in cron.items():
                bee_slug = self.slug_map["bee"][bee_id]
                bee = flat[cron_slug][bee_slug]

                if cron_slug not in hive:
                    hive[cron_slug] = {}

                if incl_duration and item['timestamp']:
                    item['duration'] = str(self.Now - item['timestamp'])

                hive[cron_slug][bee_slug] = item

        return hive

    def get_control(self):
        """Get wurker_control record."""

        log("Obtaining Wurker Control from DB", level=logging.DEBUG)

        with self.session() as db:
            control = db.query(WurkerControl).filter(
                WurkerControl.app_id == self.app_id
            ).one()

        if control is None:
            raise Exception("Unable to obtain wurker_control record")

        return control

    def set_control(self, reload=None, pause=None):
        """Nones will not be updated. Only strict booleans."""

        if not isinstance(reload, bool) and not isinstance(pause, bool):
            return

        log("Setting Wurker Control in DB", level=logging.DEBUG)

        with self.session() as db:
            control = db.query(WurkerControl).filter(
                WurkerControl.app_id == self.app_id
            ).one()

            if control is None:
                raise Exception("Unable to obtain wurker_control record")

            if isinstance(reload, bool):
                control.reload = reload
            if isinstance(pause, bool):
                control.pause = pause

            db.commit()

    def run_once(self):
        """Run the bees once through for any the CLI options."""

        bees, _ = self.get_bees()
        if not len(bees):
            return "Wurker Bee(s) not found"

        hive = {"ondemand":{}, "supervised":{}}
        if self.args.ondemand:
            hive["ondemand"] = bees["ondemand"]
        elif self.args.scheduled or self.args.supervised:
            key = "scheduled" if self.args.scheduled else "supervised"
            hive["supervised"] = bees[key]
        else:
            hive["supervised"] = bees["supervised"]
            hive["supervised"].update(bees["scheduled"])
            hive["ondemand"] = bees["ondemand"]

        bee_ct = 0
        for wurk_type, wurk_cfg in hive.items():
            for cron_slug, bee_cfg in wurk_cfg.items():
                for bee_slug, bee in bee_cfg.items():
                    bee.run_once = True
                    bee.cron = None
                    bee_ct += 1

        self.bees = hive
        self.bee_ct = bee_ct
        if not self.bee_ct:
            return f"No Wurker Bee(s) found matching criteria"

        log(f"Running ({self.bee_ct}) Wurker Bee(s) one time")

        errno = None
        try:
            while not Globals.STOP:
                if self.bees["supervised"]:
                    self._run_supervised(run_once=True)
                if self.bees["ondemand"]:
                    self._run_ondemand(run_once=True)
                time.sleep(1)

        except KeyboardInterrupt:
            errno = "\nKeyboard interrupt detected!"
        except Exception as ex:
            traceback.print_exc()
            errno = ex
        finally:
            self.kill()

        log("Wurk is COMPLETED")

    def run_forever(self):
        """Run the bees forever."""

        errno = None

        # Clear the Hive before running forever
        self.hive.clear()

        # Initial gets. We'll get again in case we need to reload
        control = self.get_control()
        if control.pause:
            errno = f"Wurker Control ({control.app_id}) sent PAUSE signal!"
            log(errno, level=logging.ERROR)
            return errno

        self.bees, self.bee_ct = self.get_bees()
        try:
            delay = 0
            while not Globals.STOP:
                if control.pause:
                    log(f"Wurker Control ({control.app_id}) sent PAUSE signal!", level=logging.ERROR)
                    Globals.PAUSE = True
                    self.kill()
                elif control.reload:
                    log(f"Wurker Control ({control.app_id}) sent RELOAD signal!", level=logging.ERROR)
                    delay = 0
                    self.kill()
                    self.bees, self.bee_ct = self.get_bees()
                    self.set_control(reload=False)
                else:
                    self._run_scheduled()
                    self._run_supervised()

                # Delay control and ondemand checks so we're not spamming the DB
                if delay % 30 == 0:
                    delay = 0
                    control = self.get_control()
                    self._run_ondemand()

                time.sleep(1.0)
                delay += 1

        except KeyboardInterrupt:
            errno = "\nKeyboard interrupt detected!"
        except Exception as ex:
            traceback.print_exc()
            errno = ex
        finally:
            self.kill()
            self.hive.clear()
            log("Unexpected exit!", level=logging.ERROR)

        return errno

    def _run_scheduled(self):
        """Run scheduled (cron) bees."""

        if Globals.PAUSE:
            return

        if not "scheduled" in self.bees or not self.bees["scheduled"]:
            return

        offset = 30
        tmp_now = self.Now
        scheduled = self.bees["scheduled"]

        for cron_slug, bee_cfg in scheduled.items():
            for bee_slug, bee in bee_cfg.items():
                if not bee.enabled:
                    continue

                # Is there a job process already?
                if bee.proc is not None:

                    # There's a process. Is the job done?
                    if bee.poll() is not None:
                        errno = bee.poll()
                        level = logging.ERROR if errno != 0 else logging.INFO

                        if not bee.kill():
                            log(f"[{bee.PID}] SCHED '{bee.slug}' job could not be killed!", level=logging.ERROR)
                            continue

                        if bee.errors:
                            log(f"[{bee.PID}] SCHED '{bee.slug}' job errors:\n\t{bee.errors}", level=logging.ERROR)

                        self.hive.delete(bee)
                        log(f"[{bee.PID}] SCHED '{bee.slug}' job COMPLETED with exit code ({errno})", level=level)

                    # Job still running... is it timing out?
                    elif tmp_now - bee.CurrRun > timedelta(seconds=bee.timeout):

                        # Exempt allowed overruns
                        if not bee.overrun or bee.runcount >= bee.overrun:
                            if not bee.kill(error_status=WurkerBeeState.TIMEOUT):
                                log(f"[{bee.PID}] SCHED '{bee.slug}' job could not be killed!", level=logging.ERROR)
                                continue

                            if bee.errors:
                                log(f"[{bee.PID}] SCHED '{bee.slug}' job errors:\n{bee.errors}", level=logging.ERROR)

                            self.hive.delete(bee)
                            log(f"[{bee.PID}] SCHED '{bee.slug}' job TIMEOUT. KILLED and RESCHEDULED @ {bee.NextRun}",
                                level=logging.ERROR)

                            bee.set_next()

                # Time to run the job?
                if tmp_now - timedelta(seconds=offset) <= bee.NextRun < tmp_now + timedelta(seconds=offset):

                    # Is job still running? This should not happen unless timeout failed/wrong or allowed overrun
                    if bee.proc is not None and bee.poll() is None:
                        error = f"[{bee.PID}] SCHED '{bee.slug}' job @ {bee.CurrRun} OVERRUN. "

                        # Let this job overrun?
                        if bee.overrun and bee.runcount <= bee.overrun:
                            log(error + f"Allowed overrun ({bee.runcount}) of ({bee.overrun})...")
                            bee.runcount += 1
                            bee.set_report(status=WurkerBeeState.OVERRUN)
                            self.hive.set_runcount(bee)
                            continue

                        # Kill and reschedule
                        pid = bee.pid

                        if not bee.kill(error_status=WurkerBeeState.TIMEOUT):
                            log(f"[{bee.PID}] SCHED '{bee.slug}' job could not be killed!", level=logging.ERROR)
                            continue

                        if bee.errors:
                            log(f"[{pid}] SCHED '{bee.slug}' job errors:\n{bee.errors}", level=logging.ERROR)

                        self.hive.delete(bee)
                        log(error + f"KILLED and RESCHEDULED @ {bee.NextRun}", level=logging.ERROR)

                        bee.set_next()
                        continue

                    # OK try to run the scheduled job
                    bee.run()
                    bee.set_next()
                    if not bee.proc:
                        log(f"SCHED '{bee.slug}' job FAILED @ {bee.CurrRun} RESCHEDULED @ {bee.NextRun}",
                            level=logging.ERROR)

                        bee.kill()

                    else:
                        log(f"[{bee.PID}] SCHED '{bee.slug}' job STARTED")
                        try:
                            self.hive.add(bee)
                        except Exception as ex:
                            log(f"SCHED '{bee.slug}' job FAILED adding to Hive: {ex}")
                            bee.kill()

    def _run_supervised(self, run_once=False):
        """Run supervised (always on) bees."""

        if Globals.PAUSE:
            return

        if not "supervised" in self.bees or not self.bees["supervised"]:
            return

        rm_bees = []
        supervised = self.bees["supervised"]
        bee_ct = sum(len(b) for b in supervised.values())
        if not bee_ct:
            if run_once:
                Globals.STOP = True
            return

        for cron_slug, bee_cfg in supervised.items():
            for bee_slug, bee in bee_cfg.items():
                if not bee.enabled:
                    continue

                action = None
                if not bee.proc:
                    level, action = (logging.INFO, "STARTED")
                elif bee.poll() is not None:
                    self.hive.delete(bee)

                    if run_once:
                        rm_bees.append((cron_slug, bee_slug))
                    elif not bee.quiet_restart:
                        log(f"[{bee.PID}] SUPER '{bee.slug}' job DIED", level=logging.ERROR)
                        level, action = (logging.ERROR, "RESTARTED")
                    else:
                        level, action = (None, "QUIET RESTART")

                if action:
                    bee.run()
                    if not bee.proc:
                        log(f"SUPER '{bee.slug}' job FAILED to run", level=logging.ERROR)

                        pid = bee.pid
                        bee.kill()
                        if bee.errors:
                            log(f"[{pid}] SUPER '{bee.slug}' job errors:\n{bee.errors}", level=logging.ERROR)

                    elif level:
                        log(f"[{bee.PID}] SUPER '{bee.slug}' job {action}", level=level)
                        self.hive.add(bee)

        # If run_once then we can delete the bees outside the loop
        for pair in rm_bees:
            self.rm_bee("supervised", *pair)

    def _run_ondemand(self, run_once=False):
        """Run ondemand (once and done) bees. Reports back to the wurker_queue table."""

        log("DEMAND check", level=logging.DEBUG)

        if Globals.PAUSE:
            log("DEMAND PAUSED", level=logging.WARN)
            return

        # Update ondemand Bees from DB
        ondemand, bee_ct = self.get_bees("ondemand")

        if bee_ct and "ondemand" in ondemand:
            if not "ondemand" in self.bees:
                self.bees["ondemand"] = {}
            if not "ondemand" in self.bees["ondemand"]:
                self.bees["ondemand"]["ondemand"] = {}

            for bee_slug, bee in ondemand["ondemand"].items():
                if bee_slug not in self.bees["ondemand"]["ondemand"]:
                    self.bees["ondemand"]["ondemand"][bee_slug] = bee

        # No ondemand? Then nothing to do...
        if not "ondemand" in self.bees or not self.bees["ondemand"]:
            return

        rm_bees = []
        tmp_now = self.Now
        ondemand = self.bees["ondemand"]
        bee_ct = sum(len(b) for b in ondemand.values())

        # No ondemand bees left? Then nothing to do...
        if not bee_ct or not "ondemand" in ondemand:
            if run_once:
                Globals.STOP = True
            return

        # OK loop over ondemand Bees...
        for bee_slug, bee in ondemand["ondemand"].items():
            if not bee.enabled:
                log(f"DEMAND '{bee.slug}' job is disabled", level=logging.DEBUG)
                continue

            if bee.queue is None:
                log(f"DEMAND '{bee.slug}' job is not queued", level=logging.DEBUG)
                continue

            # Is there a job process already?
            if bee.proc is not None:

                # There's a process. Is the job done?
                if bee.poll() is not None:
                    errno = bee.poll()

                    level = logging.ERROR if errno != 0 else logging.INFO
                    log(f"[{bee.PID}] DEMAND '{bee.slug}' job COMPLETED with exit code ({errno})", level=level)

                    pid = bee.pid
                    bee.kill()
                    self.hive.delete(bee)
                    if bee.errors:
                        log(f"[{pid}] DEMAND '{bee.slug}' job errors:\n{bee.errors}", level=logging.ERROR)

                    rm_bees.append(bee)

                # Job still running... is it timing out?
                elif bee.timeout and tmp_now - bee.start_dt > timedelta(seconds=bee.timeout):
                    log(f"[{bee.PID}] DEMAND '{bee.slug}' job TIMEOUT. KILLED", level=logging.ERROR)

                    bee.set_report(status=WurkerBeeState.ERROR, status_info={
                        "errors":["Timeout after {bee.timeout} seconds"]
                    })

                    pid = bee.pid
                    if not bee.kill(error_status=WurkerBeeState.TIMEOUT):
                        log(f"[{bee.PID}] SCHED '{bee.slug}' job could not be killed!", level=logging.ERROR)
                        continue

                    self.hive.delete(bee)

                    if bee.errors:
                        log(f"[{pid}] DEMAND '{bee.slug}' job errors:\n{bee.errors}", level=logging.ERROR)

                    rm_bees.append(bee)

            # No process, so run the job
            else:
                bee.run()
                if not bee.proc:
                    log(f"DEMAND '{bee.slug}' job FAILED", level=logging.ERROR)
                    bee.set_report(status=WurkerBeeState.ERROR, status_info={"errors": ["Failed to start"]})
                    bee.kill()
                else:
                    log(f"[{bee.PID}] DEMAND '{bee.slug}' job STARTED")
                    self.hive.add(bee)

        # OK now that we're not in the loop we can unset any expired bees
        for bee in rm_bees:
            self.rm_bee("ondemand", "ondemand", bee.slug)

    def kill(self, bees:list=[]):
        """Kill some or all of the busy bees."""

        bees = bees or self.bees
        if not len(bees):
            return "Wurker Bee(s) not found"

        for wurk_type, wurk_cfg in bees.items():
            for cron_slug, bee_cfg in wurk_cfg.items():
                for bee_slug, bee in bee_cfg.items():
                    pid = bee.pid
                    bee.kill()
                    self.hive.delete(bee)

                    if pid:
                        level, action = (
                            (logging.ERROR, "KILLED") if not bee.run_once
                            else (logging.INFO, "COMPLETED")
                        )
                        log(f"[{pid}] '{bee.slug}' job {action}", level=level)

                        if bee.errors:
                            log(f"[{pid}] '{bee.slug}' job errors:\n{bee.errors}", level=logging.ERROR)

        # Finally, clear the hive
        self.hive.clear()
