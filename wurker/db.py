import os
import re
import sys
import json
import uuid
import warnings

from dataclasses import asdict

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.sql import text, select
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils.functions import database_exists, create_database

from models.wurker import WurkerControl, WurkerCron, WurkerBee, WurkerCommand, WurkerQueue, WurkerStatus


def make_engine(db_conf:object=None, echo:bool=False, connect_args:dict={}, pool_recycle:int=600, **kwargs) -> object:
    """See the list below for required elements of kwargs."""

    if db_conf and not kwargs:
        kwargs = {k:v for k,v in asdict(db_conf).items() if v}

    # File URL for sqlite
    if "url" in kwargs and kwargs["url"]:
        return create_engine(kwargs["url"], connect_args=connect_args)

    # OR build your own DSN and pass it in. Or pass the pieces in and build it here
    if "dsn" in kwargs and kwargs["dsn"]:
        dsn = kwargs["dsn"]
    else:
        req_db_keys = ["dbdriver", "username", "password", "hostname", "port", "database",]
        if any([k not in kwargs or kwargs[k] is None or kwargs[k]=='' for k in req_db_keys]):
            raise Exception("Missing one or more required database settings")

        dsn = "{dbdriver}://{username}:{password}@{hostname}:{port}/{database}".format(**kwargs)

    pool_recycle = pool_recycle if "dbdriver" in kwargs and kwargs["dbdriver"] == "mysql" else -1

    engine = None
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        engine = create_engine(
            dsn,
            connect_args=connect_args,
            pool_recycle=pool_recycle,
            convert_unicode=True,
            encoding='utf8',
            echo=echo,
        )

    if engine is None:
        raise Exception(f"Unable to connect to config DB: {kwargs['hostname']}")

    return engine


def make_session(db_conf:object, **kwargs) -> object:
    """Runs connect() then returns a session object."""

    engine = make_engine(db_conf=db_conf, **kwargs)
    session = sessionmaker(bind=engine)
    return session


def schema_exists(db_conf:object, base:object, return_bool:bool=False, **kwargs):
    """Checks that the Wurker schema and all it's tables exist. Raises exception if not."""

    engine = make_engine(db_conf=db_conf, **kwargs)

    try:
        if not database_exists(engine.url):
            if return_bool:
                return False
            raise Exception(f"Database does not exist: '{engine.url}'")
    except Exception as ex:
        if return_bool:
            return False
        raise ex

    for table in base.metadata.sorted_tables:
        if not sqlalchemy.inspect(engine).has_table(table):
            if return_bool:
                return False
            raise Exception(f"Table does not exist: '{table}'")

    return True


def create_schema(db_conf:object, base:object, **kwargs) -> bool:
    """Installs the database schema if it does not exist."""

    engine = make_engine(db_conf=db_conf, **kwargs)

    if not database_exists(engine.url):
        create_database(engine.url)

    if not database_exists(engine.url):
        return False

    # Schema exists, so now create tables
    base.metadata.create_all(engine)
    return True


def drop_schema(db_conf:object, base:object, **kwargs) -> bool:
    """Drops just the Wurker tables if they exist."""

    engine = make_engine(db_conf=db_conf, **kwargs)

    if not database_exists(engine.url):
        return True

    # Clear the queue to avoid FK constraints
    with engine.connect() as conn:
        conn.execute(WurkerQueue.__table__.delete())

    # Schema exists, so drop everything
    base.metadata.drop_all(engine)
    return True


def seed_control(db_conf:object, app_id:str, **kwargs) -> bool:
    """Insert-get a wurker_control record by ID, and punnily enough, cede engine."""

    engine = make_engine(db_conf=db_conf, **kwargs)

    with engine.connect() as conn:
        stmt = select(WurkerControl).where(WurkerControl.app_id == app_id)
        control = conn.execute(stmt).first()
        if control is None:
            stmt = WurkerControl.__table__.insert().values(app_id=app_id)
            control = conn.execute(stmt)

    if control is None:
        return False

    return True


def get_control(db_conf:object, app_id:str, engine:object=None, as_dict:bool=False, **kwargs):
    """Helper to get control data."""

    engine = engine or make_engine(db_conf=db_conf, **kwargs)

    with engine.connect() as conn:
        stmt = select(WurkerControl).where(WurkerControl.app_id == app_id)
        control = conn.execute(stmt).first()

    if control is None:
        return None

    if as_dict:
        return {
            "app_id": control.app_id,
            "seed": control.seed,
            "reload": control.reload,
            "pause": control.pause,
        }

    return control


def set_control(db_conf:object, app_id:str, engine:object=None, key:str='', value:str=''):
    """Helper to set control data."""

    engine = engine or make_engine(db_conf=db_conf, **kwargs)

    kwargs = {key:value}
    with engine.connect() as conn:
        stmt = WurkerControl.__table__.update().where(
            WurkerControl.app_id == app_id
        ).values(**kwargs)

        conn.execute(stmt)


def seed_data(db_conf:object, app_id:str, data_path:str, app_root:str, **kwargs) -> bool:
    """Inserts data from a JSON or SQL file."""

    session = make_session(db_conf, **kwargs)
    with session() as db:
        control = get_control(db_conf, app_id, engine=db.get_bind())

        pattern = r"seed_data_(?P<seed>\d{3}).(?P<ext>(json|sql))"
        match = re.match(pattern, os.path.basename(data_path))
        if not match:
            return None, f"Unrecognized seed data file: '{data_path}'", None
        if control.seed >= int(match["seed"]):
            return None, None, None

        seed = int(match["seed"])

        # Is this a SQL file?
        if match["ext"].lower() == "sql":
            if seed_raw_sql(db_conf, data_path, **kwargs):
                set_control(db_conf, app_id, engine=db.get_bind(), key="seed", value=seed)
                return seed, None, None
            else:
                seed, f"Unable to seed from SQL file: '{data_path}'"

        # Otherwise it's JSON, and we add with ORM
        with open(data_path) as fh:
            seed_data = json.load(fh)

        command_data = seed_data.get("wurker_command", {})
        bee_data = seed_data.get("wurker_bee", {})
        cron_data = seed_data.get("wurker_cron", {})
        bee_cron_data = seed_data.get("wurker_bee_cron", {})

        if not command_data and not bee_data and not cron_data:
            return seed, f"Missing command, cron, and bee data", None

        # Track warnings
        warnings = []

        # We will re-use existing commands by slug
        existing_commands = db.query(WurkerCommand).filter(WurkerCommand.app_id == app_id).all()
        commands = {c.slug:c for c in existing_commands}
        cmd_fields = [c.key for c in WurkerCommand.__table__.columns if c.key not in ["id", "app_id"]]

        for cmd_cfg in command_data:

            # TODO Validate data
            invalid = [k for k in cmd_cfg if k not in cmd_fields]
            if invalid:
                return seed, f"Invalid command field(s): '{invalid}'", None

            missing = [k for k in cmd_fields if k not in cmd_cfg]
            if missing:
                return seed, f"Missing required command field(s): '{missing}'", None

            # Skip if already configured (for now)
            if cmd_cfg["slug"] in commands:
                warnings.append((seed, f"Already set command: '{cmd_cfg['slug']}'"))
                continue

            # If not is_module, we may resolve the abspath
            if not cmd_cfg["is_module"] and cmd_cfg["command"].startswith("./scripts/"):
                cmd_cfg["command"] = os.path.join(app_root, cmd_cfg["command"].replace("./scripts/", "scripts/"))

            cmd_cfg["app_id"] = app_id
            command = WurkerCommand(**cmd_cfg)
            db.add(command)
            commands[command.slug] = command

        # We will re-use existing crons by slug
        existing_crons = db.query(WurkerCron).filter(WurkerCron.app_id == app_id).all()
        crons = {c.slug:c for c in existing_crons}
        cron_fields = [c.key for c in WurkerCron.__table__.columns if c.key not in ["id", "app_id"]]

        for cron_cfg in cron_data:

            # TODO Validate data
            invalid = [k for k in cron_cfg if k not in cron_fields]
            if invalid:
                return seed, f"Invalid cron field(s): '{invalid}'", None

            missing = [k for k in cron_fields if k not in cron_cfg]
            if missing:
                return seed, f"Missing required cron field(s): '{missing}'", None

            # Skip if already configured (for now)
            if cron_cfg["slug"] in crons:
                warnings.append((seed, f"Already set cron: '{cron_cfg['slug']}'"))
                continue

            cron_cfg["app_id"] = app_id
            cron = WurkerCron(**cron_cfg)
            db.add(cron)
            crons[cron.slug] = cron

        # Now we can relate...
        existing_bees = db.query(WurkerBee).filter(WurkerBee.app_id == app_id).all()
        bees = {b.slug:b for b in existing_bees}
        bee_fields = [
            c.key for c in WurkerBee.__table__.columns
            if c.key not in ["id", "app_id", "command_id", "quiet_restart"]
        ]
        bee_fields.append("command_slug")

        for bee_cfg in bee_data:

            # TODO Validate data
            invalid = [k for k in bee_cfg if k not in bee_fields + ["quiet_restart"]]
            if invalid:
                return seed, f"Invalid bee field(s): '{invalid}'", None

            missing = [k for k in bee_fields if k not in bee_cfg]
            if missing:
                return seed, f"Missing required bee field(s): '{missing}'", None

            # Skip if already configured (for now)
            if bee_cfg["slug"] in bees:
                warnings.append((seed, f"Already set bee: '{bee_cfg['slug']}'"))
                continue

            # We use special 'command_slug' pseudo-field to specify the Command
            command_slug = bee_cfg["command_slug"]
            if command_slug not in commands:
                db.rollback()
                return seed, f"Unknown command slug: '{command_slug}'", None

            del bee_cfg["command_slug"]
            bee_cfg["app_id"] = app_id

            try:
                bee = WurkerBee(**bee_cfg)
                bee.command = commands[command_slug]
            except Exception as ex:
                db.rollback()
                return seed, f"Exception: {ex}", None

            if bee.slug in bee_cron_data:

                cron_slug = bee_cron_data[bee.slug]
                if cron_slug not in crons:
                    db.rollback()
                    return seed, f"Unknown cron slug: '{cron_slug}'", None

                bee.crons = [crons[cron_slug]]

            db.add(bee)

        db.commit()
        set_control(db_conf, app_id, engine=db.get_bind(), key="seed", value=seed)

    return seed, None, warnings


def seed_raw_sql(db_conf:object, data_path:str, **kwargs) -> bool:
    """Inserts data from a SQL file."""

    engine = make_engine(db_conf=db_conf, echo=True, **kwargs)
    with engine.connect() as conn:
        with open(data_path) as fh:
            stmts = [s for s in re.split(r';\s*$', fh.read(), flags=re.MULTILINE) if s]
            for stmt in stmts:
                try:
                    conn.execute(text(stmt))
                except Exception:
                    return False

    return True


def update_status(db_conf:object, app_id:str, status:dict, **kwargs):
    """
    Insert or update 'wurker_status' for the given app ID and data from 'status' subcommand.
    Raises an Exception on error instead of returning True/False.
    """

    required = ["app_id", "name", "slug", "is_queen", "env",]
    allowed = ["private_ip", "public_ip", "server_domain",]

    # Normalize the data and throw exceptions for missing required keys
    data = {}
    for key in required:
        if key not in status:
            raise TypeError(f"Missing required key in 'status' parameter: '{key}'")

        data[key] = status[key]

    data.update({k:status[k] for k in allowed if k in status})

    # OK we can upsert, now
    session = make_session(db_conf, **kwargs)
    with session() as db:
        existing = db.query(WurkerStatus).filter(WurkerStatus.app_id == data["app_id"]).first()

        if existing is None:
            insert = WurkerStatus(**data)
            db.add(insert)
        else:
            for key, value in data.items():
                setattr(existing, key, value)

        db.commit()


def get_drone_status(db_conf:object, app_id:str, drones:list=[], **kwargs):
    """Obtain wurker_status records for the Drone(s) belonging to the given Queen app_id."""

    session = make_session(db_conf=db_conf, **kwargs)
    with session() as db:

        # Filter by Drone slug or ID?
        drone_ids = []
        for item in drones:
            drone_id = None
            try:
                uuid.UUID(item)
            except ValueError:
                try:
                    drone_id = db.query(WurkerControl).join(
                        WurkerStatus, WurkerStatus.app_id == WurkerControl.app_id
                    ).filter(
                        WurkerStatus.slug == item
                    ).one().app_id

                except:
                    raise Exception(f"Drone slug '{item}' not found for this Queen")

            else:
                try:
                    drone_id = db.query(WurkerControl).filter(WurkerControl.app_id == item).one().app_id
                except:
                    raise Exception(f"ERROR: Drone ID '{item}' not found for this Queen")

            if not drone_id:
                raise Exception(f"Drone identifier '{item}' not recognized", file=sys.stderr)

            drone_ids.append(drone_id)

        # ... then use those IDs to filter our query ...
        query = db.query(WurkerControl).filter(WurkerControl.app_id != app_id)
        if len(drone_ids):
            query = query.filter(WurkerControl.app_id.in_(drone_ids))

        drone_control = query.all()
        if not len(drone_control):
            print(f"WARNING: Queen ({app_id}) has no Drones in her Colony", file=sys.stderr)
            return None

        # ... and finally use the filtered controls to get drone statuses from DB
        drone_status = db.query(WurkerStatus).filter(
            WurkerStatus.app_id.in_([c.app_id for c in drone_control]),
            WurkerStatus.is_queen != True
        ).all()

        if not len(drone_status):
            return None

        return drone_status
