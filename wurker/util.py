import json
import shlex
import datetime

from json import JSONEncoder
from dataclasses import dataclass

import settings
from models.wurker import WurkerBee, WurkerCron, WurkerCommand, FILLMAP


class DateTimeEncoder(JSONEncoder):
    """Encodes datetime objects into ISO format in json.dump methods"""

    def default(self, obj):
        if isinstance(obj, (datetime.date, datetime.datetime)):
            return obj.isoformat()


def str_to_datetime(dt_str:str):
    """Tries several several ISO-based datetime formats to get datetime object."""

    formats = [
        "%Y-%m-%dT%H:%M:%S%z",    # ISO with UTC offset
        "%Y-%m-%dT%H:%M:%S",      # ISO with no offset
        "%Y-%m-%dT%H:%M:%S.%f",   # ISO with microsec and no offset
        "%Y-%m-%dT%H:%M:%S.%f%z", # ISO with microsec and UTC offset
        "%Y-%m-%d %H:%M:%S%z",    # MySQL with UTC offset
        "%Y-%m-%d %H:%M:%S",      # MySQL with no offset
        "%Y-%m-%d %H:%M:%S.%f",   # MySQL with microsec and no offset
        "%Y-%m-%d %H:%M:%S.%f%z", # MySQL with microsec and UTC offset
        "%m/%d/%Y",               # US date with no time and 4-digit year
        "%m/%d/%y",               # US date with no time and 2-digit year
        "%Y-%m-%d",               # ISO date with no time and 4-digit year
    ]

    dt = None
    for fmt in formats:
        try:
            dt = datetime.datetime.strptime(dt_str, fmt)
        except ValueError:
            continue

    # Couldn't parse it? Then make this function act like strptime()
    if dt is None:
        raise ValueError("time data '{0}' does not match any known format".format(dt_str))

    return dt


@dataclass
class BotArgs:
    """Encapsulates all possible arguments like a rigid argparse.Namespace."""

    # Need to keep local paths straight
    app_root: str = None
    subcommand: str = None

    # Serve-specific
    return_result: bool = True

    # Logging stuff
    log: str = "syslog"
    loglevel: str = "debug"

    # Show command
    command: str = None
    bee: str = None
    cron: str = None
    supervised: bool = False
    scheduled: bool = False
    ondemand:bool = False
    hive: bool = False
    any: bool = True
    widowed: bool = False

    # Edit command
    option: str = None
    key: list = None
    json: str = None

    # Control command
    action: str = None

    # Report command
    start: str = None
    end: str = None
    page: int = None
    page_limit: int = None
    sort: str = None
    sort_order: str = None

    # Colony option
    drone: str = None

    def __post_init__(self):
        """Set filter args from flags."""

        options = []
        keyed = []

        if self.subcommand == "show":
            options = ["supervised", "scheduled", "ondemand", "hive", "command", "cron", "bee",]
            keyed = ["command", "cron", "bee",]
        elif self.subcommand == "kill":
            options = ["supervised", "scheduled", "ondemand", "hive", "cron", "bee",]
            keyed = ["cron", "bee",]

        for option in options:
            value = getattr(self, option)
            if value:
                self.option = option
                self.key = True if option not in keyed else value


def parse_fillable(wurkman, enforce_required=False):
    """For add and edit functions."""

    model = FILLMAP[wurkman.args.option]["model"]
    fillable = FILLMAP[wurkman.args.option]["fillable"]
    required = FILLMAP[wurkman.args.option]["required"]

    # Validate CLI args against FILLMAP, and cast to the correct type
    kwargs = {}
    for key, val in wurkman.args.cliargs.items():
        if key not in fillable:
            raise ValueError(f"Invalid COLUMN value '{key}'. Use one of: {list(fillable.keys())}")

        if key == "args":
            kwargs[key] = val if isinstance(val, list) or isinstance(val, dict) else shlex.split(val)
        elif fillable[key] == bool:
            kwargs[key] = val if isinstance(val, bool) else True if val.lower() in ["1", "true"] else False
        else:
            kwargs[key] = fillable[key](val)

    # Bees must have a command_id or command_slug
    if wurkman.args.subcommand == "add" and wurkman.args.option == "bee" and "command_id" not in kwargs:
        if "command_slug" not in kwargs:
            raise ValueError(f"Bees require 'command_id' or 'command_slug' COLUMN key-value pair")

        with wurkman.session() as db:
            command = db.query(WurkerCommand).filter(
                WurkerCommand.slug == kwargs["command_slug"],
                WurkerCommand.app_id == wurkman.app_id
            ).first()

        if command is None:
            raise ValueError(f"Wurker command not found: '{kwargs['command_slug']}'")

        kwargs["command_id"] = command.id
        del kwargs["command_slug"]

    if enforce_required:
        missing = required - set(kwargs.keys())
        if missing:
            raise ValueError(f"Missing required COLUMN(s): {missing}")

    kwargs["app_id"] = wurkman.app_id
    return model, kwargs


# Logging helpers ##############################################################

import os
import logging
from logging.handlers import SysLogHandler, RotatingFileHandler

loglevels = {"debug": logging.DEBUG, "info": logging.INFO, "error": logging.ERROR,}
logger = logging.getLogger("Wurker")

def init_logger(args):
    """Initialize the logger based on CLI args."""

    if isinstance(args.loglevel, str):
        args.loglevel = loglevels[args.loglevel]

    logger.setLevel(args.loglevel)

    handler = (
        logging.StreamHandler() if args.log == "stdout"
        else SysLogHandler(address="/dev/log") if args.log == "syslog"
        else RotatingFileHandler(args.log, maxBytes=10*1024*1024, backupCount=5)
    )

    datefmt = "%b %d %H:%M:%S"
    logtag = settings.env.name or "Wurker"
    msgfmt = (
        "{logtag}[%(process)s]: %(levelname)s %(message)s".format(logtag=logtag) if args.log == "syslog"
        else "%(asctime)s {logtag}[%(process)s]: %(levelname)s %(message)s".format(logtag=logtag)
    )
    formatter = logging.Formatter(msgfmt, datefmt=datefmt)
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def log(msg:str, level=logging.INFO):
    """Convenience function to log the message."""

    if level == logging.ERROR:
        logger.error(msg)
    elif level == logging.DEBUG:
        logger.debug(msg)
    else:
        logger.info(msg)
