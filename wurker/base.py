import os
import sys
import time
import json
import shlex
import errno
import logging
import traceback
import importlib
import importlib.util
import subprocess

from croniter import croniter
from datetime import datetime
from sqlalchemy.sql import null
from multiprocessing import Process

import settings
from wurker.util import log
from models.wurker import WurkerBeeReport, WurkerBeeState


class BaseBee:
    """Base class for Wurker Bees. Implement if you want, but it's basic."""

    # Hive process stuff
    pid = None
    last_pid = None
    proc = None
    errors = None
    output = None
    start_dt = None
    runcount = 0
    proc_timeout = 60

    # The DB session
    session = None

    # The DBee object and optional DB cron object
    dbee = None
    dcron = None

    # Straight from the DBee
    id = None
    app_id = ""
    name = ""
    slug = ""
    description = ""
    command_id = None
    args = None
    quiet_restart = False
    enabled = True

    # Depending on wurker_command
    module = None
    command = None
    command_slug = ""

    # If scheduled, from the DBee or its cron
    cron_id = None
    cron_slug = ""
    cron = ""
    overrun = 0  # TODO

    # If scheduled, computed from cron
    timeout = None
    itercron = None
    curr_run = None
    next_run = None

    # Local param, needed to run once from CLI
    run_once = False

    # The wurker_bee_queue object for this run
    queue = None

    def __init__(self, session, dbee, dcron=None, args=None):
        """Init from DBee and related objects."""

        self.session = session
        self.dbee = dbee
        self.dcron = dcron

        # Init Bee attributes from DBee
        self.id = dbee.id
        self.app_id = dbee.app_id
        self.name = dbee.name
        self.slug = dbee.slug
        self.description = dbee.description
        self.command_id = dbee.command_id
        self.args = args or dbee.args
        self.quiet_restart = dbee.quiet_restart
        self.enabled = dbee.enabled

        self.command_slug = dbee.command.slug
        if dbee.command.is_module:
            self.module = dbee.command.command
        else:
            self.command = dbee.command.command

        if dbee.queue is not None:
            self.queue = dbee.queue

        # Additional inits
        self._init_cron()
        self._init_timeout()

    def _init_cron(self):
        """Init self-scheduling iteration from cron, if it exists."""

        if self.dbee is None or self.dcron is None:
            return

        # Assign the actual cron string like '0/5 * * * *' to the 'cron' attribute
        self.cron_id = self.dcron.id
        self.cron_slug = self.dcron.slug
        self.cron = self.dcron.cron
        self.overrun = self.dcron.overrun

        if self.cron is None:
            return

        self.itercron = croniter(self.cron, self.Now)
        if not self.next_run:
            self.set_next()

    def _init_timeout(self):
        """If None, then a reasonable default timeout based on itercron interval."""

        if isinstance(self.timeout, int) or not self.cron:
            return

        itercron = croniter(self.cron, self.Now)
        next_0 = itercron.get_next(datetime)
        next_1 = itercron.get_next(datetime)
        duration = int((next_1 - next_0).total_seconds())

        # Buffer: 10 sec for < 5 min; 30 sec for >= 5 min (300/10=30); 1 min for >= 10 min (600/10=60)
        buff = (
            10 if duration < 300 else
            30 if 300 <= duration < 600 else
            60
        )
        self.timeout = duration - buff

    def reset_proc(self):
        """Reset process attrs."""

        self.proc = None
        self.last_pid = self.pid
        self.pid = None

    @property
    def PID(self):
        return self.pid or self.last_pid

    @property
    def Now(self):
        return datetime.now()

    @property
    def CurrRun(self):
        """A property for curr_run so it looks like NextRun."""

        return self.curr_run

    @property
    def NextRun(self):
        """A property to make sure next_run is populated."""

        if not self.next_run:
            self.next_run = self.itercron.get_next(datetime)

        return self.next_run

    def set_next(self):
        """Advance the iteration over croniter, saving current run."""

        self.curr_run = self.next_run
        self.next_run = self.itercron.get_next(datetime)

    @property
    def AsDict(self):
        """Return the Bee config as a dict"""

        return {
            "id": self.id,
            "app_id": self.app_id,
            "name": self.name,
            "slug": self.slug,
            "description": self.description,
            "command_id": self.command_id,
            "command_slug": self.command_slug,
            "args": self.args,
            "timeout": self.timeout,
            "quiet_restart": self.quiet_restart,
            "enabled": self.enabled,
            "module": self.module.__name__ if self.module else None,
            "command": self.command,
            "cron_id": self.cron_id,
            "cron": self.cron,
        }

    def log(self, msg, exception=False, level=logging.INFO):
        """Adds Bee-specific metadata to log entries."""

        msg = f"[{self.slug}] {msg}"
        if exception:
            level = logging.ERROR
            msg += "\n{0}".format(traceback.format_exc())

        log(msg, level=level)

    def set_report(self, status=WurkerBeeState.PROCESSING, status_info=None):
        """Set report status, possibly creating a new report record first."""

        with self.session() as db:
            report = db.query(WurkerBeeReport).filter(
                WurkerBeeReport.app_id == self.app_id,
                WurkerBeeReport.wurker_bee_id == self.id,
                WurkerBeeReport.wurker_cron_id == self.cron_id,
                WurkerBeeReport.hive_pid == self.pid,
            ).first()

            if report is None:
                report = WurkerBeeReport(
                    app_id = self.app_id,
                    wurker_bee_id = self.id,
                    wurker_cron_id = self.cron_id,
                )
                db.add(report)

            report.status = status
            self._update_hive_info(report)

            # The status_info column is JSON datatype, so needs special handling
            report_info = self._update_status_info(report, status_info)
            report.status_info = null()
            db.flush()
            report.status_info = report_info

            # Notice we didn't commit until now!
            db.commit()

    def _update_hive_info(self, report):
        """Particulars about this Hive run of the Bee. We ignore duration and instead compute it."""

        if not report or self.pid is None:
            return

        # Compute duration if hive_info already set
        if report.hive_pid is not None and report.hive_timestamp is not None:
            report.hive_duration = (self.Now - report.hive_timestamp).total_seconds()

        # TODO Compute hive values now
        report.hive_pid = self.pid
        report.hive_timestamp = self.Now       # Important: we did this AFTER setting duration!
        report.hive_runcount = self.runcount

    def _update_status_info(self, report, status_info):
        """Just easier to return from a little function than nest if-elifs."""

        report_info = report.status_info or {}

        if not report_info:
            report_info = status_info or {"runcount": self.runcount}

        if not status_info:
            return report_info

        # Info we might wanna update in-place...
        if "errors" in status_info:
            if "errors" not in report_info:
                report_info["errors"] = status_info["errors"]
            else:
                report_info["errors"].extend(status_info["errors"])

        if "runcount" in status_info:
            report_info["runcount"] = status_info["runcount"]
        else:
            report_info["runcount"] = self.runcount

        # Arbitrary values...
        for key, value in status_info.items():
            if key in ["errors", "runcount"]:
                continue

            report_info[key] = value

        # JSON datatype so we return the data to be nullified, flushed and set again
        return report_info

    def run(self):
        raise NotImplementedError

    def poll(self):
        raise NotImplementedError

    def wait(self, timeout=None):
        raise NotImplementedError

    def kill(self):
        raise NotImplementedError

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)


class Bee(BaseBee):
    """Base class for external, arbitrary Wurker Bees."""

    def __init__(self, session, dbee, dcron=None, args=None):
        """Get ready to run an arbitrary command."""

        BaseBee.__init__(self, session, dbee, dcron=dcron, args=args)
        self._init_command()

    def _init_command(self):
        """Validate we can execute the command."""

        self.command = shlex.split(self.command)

        # In case of 'language command' form, we still need an absolute path
        cmd_path = self.command[0] if len(self.command) == 1 else self.command[1]
        if not os.path.exists(cmd_path):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), cmd_path)

        if self.args and isinstance(self.args, str):
            self.args = shlex.split(self.args)

    def run(self):
        """That's called self-commenting code right there."""

        self.start_dt = self.Now

        # Still a process running?
        if self.proc and self.proc.poll() is None:
            return False

        # TODO Capture stderr
        stdout = subprocess.PIPE
        stderr = subprocess.PIPE

        args = self.command + self.args
        self.proc = subprocess.Popen(args, stdout=stdout, stderr=stderr, text=True)
        self.pid = self.proc.pid
        self.runcount = 1

        # Update to PROCESSING
        self.set_report(status=WurkerBeeState.PROCESSING)

        if not self.quiet_restart:
            self.log(f"[{self.pid}] Running Wurker Bee '{self.slug}'...")

        return True

    def poll(self):
        """Convenience wrapper around subprocess poll()."""

        if not self.proc:
            self.reset_proc()
            return None

        return self.proc.poll()

    def wait(self):
        """Convenience wrapper around subprocess wait()."""

        if not self.proc:
            self.reset_proc()
            return None

        return self.proc.wait(timeout=self.proc_timeout)

    def kill(self, error_status=WurkerBeeState.ERROR):
        """Kill everyone. Returns boolean."""

        is_dead = True

        if not self.proc:
            self.set_report(status=WurkerBeeState.COMPLETED)

        else:
            try:
                output, errors = self.proc.communicate(timeout=self.proc_timeout)
            except subprocess.TimeoutExpired as sp_te:
                traceback.print_exc()
                return False

            self.output = output.strip() if output is not None else None
            self.errors = errors.strip() if errors is not None else None

            if self.proc.poll() is not None:
                if self.proc.returncode or self.errors:
                    self.set_report(status=error_status, status_info={
                        "exit_code": int(self.proc.returncode), "errors": [self.errors]
                    })
                else:
                    self.set_report(status=WurkerBeeState.COMPLETED)

            else:
                self.log(f"[{self.pid}] Sending SIGTERM to Wurker Bee '{self.slug}'...",
                         level=logging.ERROR)

                self.proc.terminate()
                time.sleep(1.0)

                # SIGTERM didn't work? Try SIGKILL
                if self.proc.poll() is None:
                    self.log(f"[{self.pid}] SIGTERM failed. Sending SIGKILL to Wurker Bee '{self.slug}'...",
                             level=logging.ERROR)

                    self.proc.kill()
                    time.sleep(1.0)

                    # Did it finally stay dead? If not... it failed to die
                    if self.proc.poll() is None:
                        self.log(f"[{self.pid}] SIGTERM and SIGKILL failed. Wurker Bee '{self.slug}' is a Zombie!",
                                 level=logging.ERROR)

                        is_dead = False

                if is_dead:
                    self.set_report(status=error_status, status_info={"message": f"Bee killed: '{self.slug}'"})
                else:
                    self.set_report(status=error_status, status_info={"message": f"Bee is zombie: '{self.slug}'"})

        # Fingers crossed!
        self.reset_proc()
        return is_dead


class PyBee(BaseBee):
    """Base class for modular Python Wurker Bees."""

    module_name = ""
    module = None

    def __init__(self, session, dbee, dcron=None, args=None):
        """Load a modular Wurker Bee."""

        BaseBee.__init__(self, session, dbee, dcron=dcron, args=args)
        self._init_module()

    def _init_module(self):
        """Load the module or raise an exception."""

        self.module_name = f"bees.{self.module}"

        if (spec := importlib.util.find_spec(self.module_name)) is not None:
            module = importlib.util.module_from_spec(spec)
            sys.modules[self.module_name] = module
            spec.loader.exec_module(module)
            self.module = module
            # self.log(f"Wurker Bee loaded: {self.module_name!r}", level=logging.DEBUG)
        else:
            raise Exception(f"Wurker PyBee not found: {self.module_name!r}")

        if not hasattr(module, "run"):
            raise Exception(f"Wurker PyBee missing 'run' function: {self.module_name!r}")

        if not self.args:
            self.args = {}
        elif isinstance(self.args, str):
            self.args = json.loads(self.args)

    def run(self):
        """Run the 'run()' module function."""

        self.start_dt = self.Now

        # Create new PENDING report record
        self.set_report()

        self.proc = Process(target=self.module.run, name=self.slug, kwargs=self.args)
        self.proc.start()
        self.pid = self.proc.pid
        self.runcount = 1

        # Update to PROCESSING
        self.set_report(status=WurkerBeeState.PROCESSING)

        self.log(f"[{self.pid}] Running Wurker PyBee '{self.slug}'...")
        return True

    def poll(self):
        """Like subprocess poll()."""

        if not self.proc:
            self.reset_proc()
            return None

        return self.proc.exitcode

    def wait(self, timeout=None):
        """Like subprocess wait()."""

        if not self.proc:
            self.reset_proc()
            return None

        return self.proc.join(timeout)

    def kill(self, error_status=WurkerBeeState.ERROR):
        """Like subprocess terminate()."""

        is_dead = True

        if not self.proc:
            self.set_report(status=WurkerBeeState.COMPLETED)

        else:
            if not self.proc.is_alive():
                if self.proc.exitcode != 0:
                    self.set_report(status=error_status, status_info={"exit_code": self.proc.exitcode})
                else:
                    self.set_report(status=WurkerBeeState.COMPLETED)

            else:
                self.log(f"[{self.pid}] Sending SIGTERM to Wurker PyBee '{self.slug}'...",
                         level=logging.ERROR)

                self.proc.terminate()
                time.sleep(1.0)

                # SIGTERM didn't work? Try SIGKILL
                if self.proc.is_alive():
                    self.log(f"[{self.pid}] SIGTERM failed. Sending SIGKILL to Wurker PyBee '{self.slug}'...",
                             level=logging.ERROR)

                    self.proc.kill()
                    time.sleep(1.0)

                    # Did it finally stay dead? If not... it failed to die
                    if self.proc.is_alive():
                        self.log(f"[{self.pid}] SIGTERM and SIGKILL failed! Wurker PyBee '{self.slug}' is a ZomBee!",
                                 level=logging.ERROR)

                        is_dead = False

                if is_dead:
                    self.set_report(status=error_status, status_info={"message": f"PyBee killed: '{self.slug}'"})
                else:
                    self.set_report(status=error_status, status_info={"message": f"PyBee is zombie: '{self.slug}'"})

        # Fingers crossed!
        self.reset_proc()
        return is_dead
