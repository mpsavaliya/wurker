import os
import sys
import glob
import importlib
import importlib.util

callbacks = {}

dispatch_root = os.path.dirname(os.path.realpath(__file__))
files = glob.glob(os.path.join(dispatch_root, "cb_*.py"))

for file_path in files:
    module_slug = os.path.basename(file_path).replace(".py", "")
    module_name = f"bees.dispatchers.{module_slug}"

    if (spec := importlib.util.find_spec(module_name)) is not None:
        module = importlib.util.module_from_spec(spec)
        sys.modules[module_name] = module
        spec.loader.exec_module(module)
    else:
        raise Exception(f"Wurker Drone Dispatcher not found: {module_name!r}")

    if not hasattr(module, "callback"):
        raise Exception(f"Wurker Drone Dispatcher missing 'callback' function: {module_name!r}")

    callbacks[module_slug] = module.callback
