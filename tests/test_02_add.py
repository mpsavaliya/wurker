import os
import pytest

from wurker.core import Wurkman
from wurker.util import BotArgs
from wurker.subcommands.add import run
from models.wurker import WurkerCommand, WurkerCron, WurkerBee

@pytest.fixture
def app_root():
    return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def test_command(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "add",
        option = "command",
        json = {
            "name": "Dummy Command",
            "slug": "dummy_command",
            "description": "A dummy command",
            "command": os.path.join(app_root, "scripts", "test_bee.sh"),
            "is_module": False,
        },
    )
    wurkman = Wurkman(args)
    run(wurkman)

    with wurkman.session() as db:
        command = db.query(WurkerCommand).filter(
            WurkerCommand.slug == "dummy_command",
            WurkerCommand.app_id == wurkman.app_id
        ).first()

        assert command is not None
        assert str(command.app_id) == wurkman.app_id
        assert command.name == "Dummy Command"
        assert command.slug == "dummy_command"
        assert command.description == "A dummy command"
        assert command.command == os.path.join(app_root, "scripts", "test_bee.sh")
        assert command.is_module == False

def test_bee(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "add",
        option = "bee",
        json = {
            "name": "Dummy Bee",
            "slug": "dummy_bee",
            "description": "A dummy bee",
            "command_slug": "dummy_command",
            "args": "-l /tmp/dummy_bee.log -i 10 -d 40",
            "quiet_restart": False,
            "enabled": True,
        }
    )
    wurkman = Wurkman(args)
    run(wurkman)

    with wurkman.session() as db:
        bee = db.query(WurkerBee).filter(
            WurkerBee.slug == "dummy_bee",
            WurkerBee.app_id == wurkman.app_id
        ).first()

        assert bee is not None
        assert str(bee.app_id) == wurkman.app_id
        assert bee.name == "Dummy Bee"
        assert bee.slug == "dummy_bee"
        assert bee.description == "A dummy bee"
        assert bee.command.slug == "dummy_command"

        assert isinstance(bee.args, list)
        assert bee.args[0] == "-l"
        assert bee.args[1] == "/tmp/dummy_bee.log"
        assert bee.args[2] == "-i"
        assert bee.args[3] == "10"
        assert bee.args[4] == "-d"
        assert bee.args[5] == "40"

        assert bee.quiet_restart == False
        assert bee.enabled == True

def test_cron(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "add",
        option = "cron",
        json = {
            "name": "Dummy Cron",
            "slug": "dummy_cron",
            "cron": "*/5 * * * *",
            "overrun": 2,
        }
    )
    wurkman = Wurkman(args)
    run(wurkman)

    with wurkman.session() as db:
        cron = db.query(WurkerCron).filter(
            WurkerCron.slug == "dummy_cron",
            WurkerCron.app_id == wurkman.app_id
        ).first()

        assert cron is not None
        assert str(cron.app_id) == wurkman.app_id
        assert cron.name == "Dummy Cron"
        assert cron.slug == "dummy_cron"
        assert cron.cron == "*/5 * * * *"
        assert cron.overrun == 2

def test_map(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "add",
        option = "map",
        json = {
            "wurker_bee_slug": "dummy_bee",
            "wurker_cron_slug": "dummy_cron",
        }
    )
    wurkman = Wurkman(args)
    run(wurkman)

    with wurkman.session() as db:
        bee = db.query(WurkerBee).filter(
            WurkerBee.slug == "dummy_bee",
            WurkerBee.app_id == wurkman.app_id
        ).first()

        assert len(bee.crons) == 1
        bee.crons[0].slug == "dummy_cron"
