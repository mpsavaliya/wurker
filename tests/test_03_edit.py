import os
import pytest

from wurker.core import Wurkman
from wurker.util import BotArgs
from wurker.subcommands.edit import run
from models.wurker import WurkerCommand, WurkerCron, WurkerBee

@pytest.fixture
def app_root():
    return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def test_command(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "edit",
        option = "command",
        key = ["dummy_command"],
        json = {"name": "Edited Command"},
    )
    wurkman = Wurkman(args)
    run(wurkman)

    with wurkman.session() as db:
        command = db.query(WurkerCommand).filter(
            WurkerCommand.slug == "dummy_command",
            WurkerCommand.app_id == wurkman.app_id
        ).first()

        assert command is not None
        assert command.name == "Edited Command"

def test_bee(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "edit",
        option = "bee",
        key = ["dummy_bee"],
        json = {"name": "Edited Bee"},
    )
    wurkman = Wurkman(args)
    run(wurkman)

    with wurkman.session() as db:
        bee = db.query(WurkerBee).filter(
            WurkerBee.slug == "dummy_bee",
            WurkerBee.app_id == wurkman.app_id
        ).first()

        assert bee is not None
        assert bee.name == "Edited Bee"

def test_cron(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "edit",
        option = "cron",
        key = ["dummy_cron"],
        json = {"name": "Edited Cron"},
    )
    wurkman = Wurkman(args)
    run(wurkman)

    with wurkman.session() as db:
        cron = db.query(WurkerCron).filter(
            WurkerCron.slug == "dummy_cron",
            WurkerCron.app_id == wurkman.app_id
        ).first()

        assert cron is not None
        assert cron.name == "Edited Cron"

def test_map(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "edit",
        option = "map",
        key = ["dummy_bee", "dummy_cron"],
        json = {
            "wurker_cron_slug": "minutes_1",
        },
    )
    wurkman = Wurkman(args)
    run(wurkman)

    with wurkman.session() as db:
        bee = db.query(WurkerBee).filter(
            WurkerBee.slug == "dummy_bee",
            WurkerBee.app_id == wurkman.app_id
        ).first()

        assert bee is not None
        assert len(bee.crons) == 1
        assert bee.crons[0].slug == "minutes_1"
